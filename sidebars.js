/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  // By default, Docusaurus generates a sidebar from the docs folder structure
  //tutorialSidebar: [{type: 'autogenerated', dirName: '.'}],

  // But you can create a sidebar manually

  tutorialSidebar: [
    {
      type: 'category',
      label: '👋 Introducción',
      link: {
        type: 'doc',
        id: 'README',
      },
      items: [
        'introduccion/desarrollo',
        {
          type: 'category',
          label: 'Integracion Continua (CI)',
          link: {
            type: 'doc',
            id: 'introduccion/integracion-continua-ci/README',
          },
          items: [
            'introduccion/integracion-continua-ci/flujo-de-trabajo',
            'introduccion/integracion-continua-ci/herramientas',
          ]
        },
        {
          type: 'category',
          label: 'Entrega Continua (CD)',
          link: {
            type: 'doc',
            id: 'introduccion/entrega-continua-cd/README',
          },
          items: [
            'introduccion/entrega-continua-cd/flujo-de-trabajo',
            'introduccion/entrega-continua-cd/herramientas',
          ]
        },
        'introduccion/monitorizacion',
      ],
    },
    {
      type: 'category',
      label: 'Infrastructura',
      link: {
        type: 'doc',
        id: 'infrastructura/README',
      },
      items: [

        {
          type: 'category',
          label: 'Gitlab',
          link: {
            type: 'doc',
            id: 'infrastructura/gitlab/README',
          },
          items: [
            'infrastructura/gitlab/ci-cd',
            'infrastructura/gitlab/runner',
          ]
        },
        {
          type: 'category',
          label: 'CertManager',
          link: {
            type: 'doc',
            id: 'infrastructura/certmanager/README',
          },
          items: [
            'infrastructura/certmanager/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'AppSec',
          link: {
            type: 'doc',
            id: 'infrastructura/appsec/README',
          },
          items: [
            'infrastructura/appsec/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'Traefik',
          link: {
            type: 'doc',
            id: 'infrastructura/traefik/README',
          },
          items: [
            'infrastructura/traefik/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'Argo CD',
          link: {
            type: 'doc',
            id: 'infrastructura/argocd/README',
          },
          items: [
            'infrastructura/argocd/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'Harbor',
          link: {
            type: 'doc',
            id: 'infrastructura/harbor/README',
          },
          items: [
            'infrastructura/harbor/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'Sonarqube',
          link: {
            type: 'doc',
            id: 'infrastructura/sonarqube/README',
          },
          items: [
            'infrastructura/sonarqube/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'Dependency-Track',
          link: {
            type: 'doc',
            id: 'infrastructura/dependency-track/README',
          },
          items: [
            'infrastructura/dependency-track/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'DefectDojo',
          link: {
            type: 'doc',
            id: 'infrastructura/defectdojo/README',
          },
          items: [
            'infrastructura/defectdojo/instalacion',
          ]
        },
        {
          type: 'category',
          label: 'GlitchTip',
          link: {
            type: 'doc',
            id: 'infrastructura/glitchtip/README',
          },
          items: [
            'infrastructura/glitchtip/instalacion',
          ]
        }
      ],
    },
    {
      type: 'category',
      label: 'Desarrollo',
      items: [{
        type: 'category',
        label: 'Git',
        link: {
          type: 'doc',
          id: 'desarrollo/git/README',
        },
        items: [
          {
            type: 'doc',
            id: 'desarrollo/git/conceptos-basicos'
          },
          {
            type: 'doc',
            id: 'desarrollo/git/git-flow'
          },
          {
            type: 'doc',
            id: 'desarrollo/git/conventionalcommits'
          },
          {
            type: 'category',
            label: 'Hooks',
            link: {
              type: 'doc',
              id: 'desarrollo/git/hooks/README',
            },
            items: [
              {
                type: 'category',
                label: 'Pre-commit',
                items: [
                  'desarrollo/git/hooks/pre-commit/php',
                ]
              }
            ]
          }
        ]
      },
      {
        type: 'category',
        label: 'Threat Modeling',
        link: {
          type: 'doc',
          id: 'desarrollo/threatmodeling/README',
        },
        items: []
      }],
    },
    {
      type: 'category',
      label: 'CI',
      link: {
        type: 'doc',
        id: 'ci/index',
      },
      items: [
        {
          type: 'category',
          label: 'Preparación',
          link: {
            type: 'doc',
            id: 'ci/preparacion/README',
          },
          items: [
            'ci/preparacion/php',
            'ci/preparacion/node',
          ]
        },
        {
          type: 'category',
          label: 'Fugas de contraseñas',
          link: {
            type: 'doc',
            id: 'ci/fugas-de-contrasenas/README',
          },
          items: [
            'ci/fugas-de-contrasenas/gitleaks'
          ]
        },
        {
          type: 'category',
          label: 'SBOM',
          link: {
            type: 'doc',
            id: 'ci/sbom/README',
          },
          items: [
            {
              type: 'category',
              label: 'CycloneDX',
              link: {
                type: 'doc',
                id: 'ci/sbom/cyclonedx/README',
              },
              items: ['ci/sbom/cyclonedx/php', 'ci/sbom/cyclonedx/node']
            }
          ]
        },
        {
          type: 'category',
          label: 'Testing',
          link: {
            type: 'doc',
            id: 'ci/testing/README',
          },
          items: ['ci/testing/php']
        },
        {
          type: 'category',
          label: 'SAST',
          link: {
            type: 'doc',
            id: 'ci/sast/README',
          },
          items: [
            'ci/sast/sonarqube'
          ]
        },
        {
          type: 'category',
          label: 'Contenedores',
          link: {
            type: 'doc',
            id: 'ci/contenedores/README',
          },
          items: [
            'ci/contenedores/linting',
            'ci/contenedores/construccion',
            'ci/contenedores/analisis',
            'ci/contenedores/publicacion',
            'ci/contenedores/firma',
          ]

        },
        {
          type: 'category',
          label: 'DAST',
          link: {
            type: 'doc',
            id: 'ci/dast/README',
          },
          items: ['ci/dast/owasp-zap']
        },
        'ci/integraciones-monitorizacion',
      ],
    },
    {
      type: 'category',
      label: 'CD ',
      link: {
        type: 'doc',
        id: 'cd/index',
      },
      items: [
        {
          type: 'category',
          label: 'GitOps',
          link: {
            type: 'doc',
            id: 'cd/gitops/README',
          },
          items: [
            'cd/gitops/jobs',
            'cd/gitops/secrets',
          ]
        },
        'cd/despliegue',
      ]
    },
    {
      type: 'category',
      label: 'Monitorización',
      link: {
        type: 'doc',
        id: 'monitorizacion/index',
      },
      items: [
        {
          type: 'category',
          label: 'Dependecy-Track',
          items: [
            'monitorizacion/dependency-track/proyecto',
          ]
        },
        {
          type: 'category',
          label: 'DefectDojo',
          items: [
            'monitorizacion/defectdojo/producto',
            'monitorizacion/defectdojo/engagement',
            'monitorizacion/defectdojo/finding',
          ]
        }
      ]
    },
  ],


};

module.exports = sidebars;
