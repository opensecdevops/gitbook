# Reflejado

## Bajo

En el primer nivel, al ingresar, nos encontramos con un campo de entrada que solicita el nombre. Al introducirlo, observamos que el sistema imprime directamente lo que se ha ingresado en el campo. Esto se debe a que el código que se ejecuta recoge la variable y la imprime directamente sin ningún proceso de validación o saneamiento. Esto puede presentar un riesgo de seguridad, ya que permite la inserción de código no deseado que se ejecutará en el navegador.

<div style={{textAlign: 'center'}}>
    ![Input XSS basico](/img/attacks/XSS/reflected/input-basic.png)
</div>

```php
<?php 

// Is there any input? 
if( array_key_exists( "name", $_GET ) && $_GET[ 'name' ] != NULL ) { 
    // Feedback for end user 
    echo '<pre>Hello ' . $_GET[ 'name' ] . '</pre>'; 
} 

?>
```

Dado este comportamiento, lo primero que haríamos para probar la vulnerabilidad a XSS sería realizar el ataque más básico de XSS, que consiste en:

```html
<script>alert(1);</script>
```

Este código intenta inyectar un script JavaScript que ejecuta una alerta en el navegador del usuario. Si al introducir este código en el campo de entrada y enviarlo, la página web muestra una alerta con el mensaje "XSS", entonces está claro que la aplicación es vulnerable a ataques de Cross-Site Scripting. Esto se debe a que la entrada del usuario no está siendo debidamente validada o saneada antes de ser renderizada en la página web.

<div style={{textAlign: 'center'}}>
    ![ataque basico](/img/attacks/XSS/reflected/attack-basic.png)
</div>

Si al insertar el código en el campo de entrada notamos que este se refleja en la barra de direcciones, indica que estamos ante un caso de XSS reflejado donde el código inyectado se transmite a través de la URL.

<div style={{textAlign: 'center'}}>
    ![ataque basico url](/img/attacks/XSS/reflected/url-basic.png)
</div>

Si compartimos este enlace con alguien y lo abre en su navegador, el código XSS que hemos introducido se ejecutará automáticamente. Esto puede resultar en que cualquier persona que haga clic en el enlace sea susceptible a ataques de robo de información, secuestro de sesión, entre otros, dependiendo de la naturaleza del código XSS utilizado. Es fundamental asegurarse de que las aplicaciones web validen y limpien correctamente todas las entradas para evitar tales vulnerabilidades.

## Medio

En el nivel medio, observamos que la vulnerabilidad XSS reflejada inicial ya no se ejecuta utilizando el método tradicional debido a una medida de mitigación implementada en el código PHP del servidor.

<div style={{textAlign: 'center'}}>
    ![Ataque basico en medio](/img/attacks/XSS/reflected/attack-basic-in-medium.png)
</div>

El código relevante que implementa esta protección es:

```php
<?php

// Is there any input?
if( array_key_exists( "name", $_GET ) && $_GET[ 'name' ] != NULL ) {
    // Get input
    $name = str_replace( '<script>', '', $_GET[ 'name' ] );

    // Feedback for end user
    echo "<pre>Hello ${name}</pre>";
}

?>
```

Esta parte del código utiliza [str_replace](http://php.net/manual/es/function.str-replace.php) para eliminar específicamente la cadena `<script>` del nombre ingresado por el usuario. Sin embargo, la función `str_replace` es sensible a mayúsculas y minúsculas, lo que significa que sólo eliminará las etiquetas `<script>` escritas exactamente de esa forma (en minúsculas).

Para probar la robustez de esta medida de seguridad, podemos intentar un ataque XSS que eluda esta mitigación cambiando el caso de las letras en la etiqueta `<script>`:

```javascript
<SCRIPT>alert("medium")</SCRIPT>
```

Al ejecutar este código, si observamos que se produce la alerta con el mensaje "medium", confirmamos que el ataque XSS ha sido exitoso.

<div style={{textAlign: 'center'}}>
    ![ataque basico](/img/attacks/XSS/reflected/attack-medium.png)
</div>

<div style={{textAlign: 'center'}}>
    ![ataque basico url](/img/attacks/XSS/reflected/url-medium.png)
</div>

Esto indica que, aunque se haya implementado una medida de seguridad, su efectividad es limitada debido a su incapacidad para manejar variaciones en el caso de las letras, lo que se traduce en una protección incompleta contra ataques XSS. Esto demuestra la importancia de implementar métodos de saneamiento más robustos y sensibles al caso, o mejor aún, enfoques más exhaustivos como el uso de listas blancas o la codificación de salida para caracteres especiales en HTML.

## Alto

En el último nivel de los ataques XSS reflejados, observamos que los métodos de inyección directa utilizando etiquetas `<script>` ya no son efectivos debido a la implementación de una expresión regular más robusta en el código PHP, que filtra eficazmente estas etiquetas sin importar las variaciones de mayúsculas y minúsculas:

<div style={{textAlign: 'center'}}>
    ![ataque alto con basico y medio](/img/attacks/XSS/reflected/attack-basic-medium-in-high.png)
</div>

```php
<?php

// Is there any input?
if( array_key_exists( "name", $_GET ) && $_GET[ 'name' ] != NULL ) {
    // Get input
    $name = preg_replace( '/<(.*)s(.*)c(.*)r(.*)i(.*)p(.*)t/i', '', $_GET[ 'name' ] );

    // Feedback for end user
    echo "<pre>Hello ${name}</pre>";
}

?>
```

Este enfoque evita efectivamente el uso de etiquetas `<script>` manipulando las letras y los espacios intermedios. No obstante, esta medida todavía deja abierta la posibilidad de utilizar otros vectores de ataque que no dependen directamente de estas etiquetas. Una técnica común en estos casos es el uso de eventos HTML que pueden desencadenar JavaScript de forma indirecta.

### Uso del atributo `onerror` en una etiqueta `<img>`

Una alternativa es explotar atributos como `onerror` en elementos HTML que ejecutan código JavaScript cuando ocurre un error, como un error al cargar una imagen:

```html
<img src=x onerror="alert('High')">
```

En este caso, dado que `"x"` no es una URL válida de imagen, el navegador intentará cargarla, fallará y luego ejecutará el código JavaScript dentro del atributo `onerror`, mostrando una alerta.

<div style={{textAlign: 'center'}}>
    ![ataque alto img](/img/attacks/XSS/reflected/attack-high.png)
</div>

### Uso de SVG para ejecución de JavaScript

Otro método eficaz es el uso de elementos SVG, que también permiten la inclusión de JavaScript:

```html
<svg/onload=alert('High2')>
```

Aquí, el evento `onload` dentro de un elemento SVG se utiliza para ejecutar JavaScript cuando el SVG se carga, lo cual sucede inmediatamente, ya que los SVG se procesan como parte del DOM.

<div style={{textAlign: 'center'}}>
    ![ataque con svg](/img/attacks/XSS/reflected/attack-high-svg.png)
</div>

Ambos métodos demuestran que, aunque las mitigaciones específicas para etiquetas `<script>` pueden ser efectivas contra ciertas formas de ataques XSS, es crucial considerar y proteger contra otros vectores que utilizan diferentes elementos y atributos HTML. Es recomendable adoptar un enfoque de seguridad en capas, que incluya tanto la validación y el saneamiento de entradas como también políticas de seguridad de contenido (CSP) que restrinjan la ejecución de scripts no autorizados.

## Ampliación de tecnicas

Para aquellos interesados en profundizar sus conocimientos sobre seguridad en aplicaciones web y específicamente en la prevención y mitigación de ataques de Cross-Site Scripting (XSS), recomiendo encarecidamente explorar dos recursos excepcionales. El [**XSS Cheat Sheet de PortSwigger**](https://portswigger.net/web-security/cross-site-scripting/cheat-sheet) ofrece una guía detallada con múltiples técnicas y ejemplos prácticos que son indispensables para entender y probar las defensas contra XSS en aplicaciones modernas. Asimismo, la [**XSS Filter Evasion Cheat Sheet de OWASP**](https://cheatsheetseries.owasp.org/cheatsheets/XSS_Filter_Evasion_Cheat_Sheet.html) proporciona un compendio exhaustivo de estrategias para evadir filtros de XSS, una herramienta crítica para diseñadores y auditores de seguridad que buscan fortalecer las aplicaciones web contra ataques maliciosos. Estas páginas son esenciales para cualquier profesional de la seguridad cibernética que busque estar al día con las prácticas más avanzadas en el campo de la seguridad web.
