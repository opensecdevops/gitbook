# XSS

XSS (Cross-site scripting) es un tipo de vulnerabilidad informática o agujero de seguridad típico de las aplicaciones web, que permite a terceros inyectar en páginas web visitadas por un usuario código JavaScript o en otro lenguaje similar (ejemplo: VBScript), eludiendo medidas de control como la Política de mismo origen.

Es posible encontrar una vulnerabilidad de Cross-Site Scripting en aplicaciones que, entre sus funciones, incluyen presentar información en un navegador web u otro contenedor de páginas web. No obstante, no se limita a sitios web disponibles en Internet, ya que también puede afectar a aplicaciones locales vulnerables a XSS, o incluso al navegador en sí.

XSS es un vector de ataque que puede ser utilizado para robar información delicada, secuestrar sesiones de usuario y comprometer el navegador, afectando la integridad del sistema. Las vulnerabilidades XSS han existido desde los primeros días de la Web.

Esta situación suele ser causada por una incorrecta validación de los datos de entrada que se utilizan en ciertas aplicaciones, o por no limpiar adecuadamente la salida para su presentación en la página web.
Esta vulnerabilidad puede manifestarse de las siguientes maneras:

- **Reflejada:** Este tipo de XSS ocurre al modificar valores que la aplicación web utiliza para pasar variables entre dos páginas, sin utilizar sesiones. Suele suceder cuando se incluye un mensaje o una ruta en la URL del navegador.
- **Persistente:** Este tipo de XSS se da comúnmente por filtrado deficiente, y consiste en insertar código HTML peligroso en sitios que lo permiten, incluyendo etiquetas como `<script>` o `<iframe>`.
- **DOM Based XSS:** Es un caso especial en el que el código JavaScript se oculta en la URL y se extrae mediante JavaScript en la página mientras se procesa, en lugar de insertarse en la página al publicarse. Esto puede hacerlo más sigiloso que otros tipos de ataques y puede no ser detectado por WAF u otras protecciones que solo analizan el cuerpo de la página.