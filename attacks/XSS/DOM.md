# DOM

## Bajo

En el nivel bajo de nuestro análisis de XSS, nos enfrentamos a la prueba de XSS basada en el DOM. Aquí, encontramos un elemento select en la interfaz de usuario que, al ser activado, genera una URL con un parámetro "default" establecido en un valor específico, en este caso, "French".

```html
http://localhost/vulnerabilities/xss_d/?default=French
```

<div style={{textAlign: 'center'}}>
    ![Input XSS basico](/img/attacks/XSS/DOM/input-dom.png)
</div>

Siguiendo el enfoque utilizado en niveles anteriores, realizamos un ataque básico de XSS agregando un script de alerta en la URL para ver cómo reacciona la aplicación. Al incluir el script en la URL como un valor para el parámetro "default", observamos que el ataque tiene éxito y se muestra una alerta en la página.

```js
http://localhost/vulnerabilities/xss_d/?default=French<script>alert("low")</script>
```

Y vemos que el ataque tiene efecto.

<div style={{textAlign: 'center'}}>
    ![Attack basico](/img/attacks/XSS/DOM/attack-basic.png)
</div>

Este resultado demuestra una vulnerabilidad de XSS persistente en el nivel bajo de la aplicación. Aunque este tipo de ataques pueden parecer simples, son una amenaza significativa cuando no se tratan adecuadamente. Identificar y remediar estas vulnerabilidades es crucial para garantizar la seguridad de las aplicaciones web contra posibles ataques maliciosos.

## Medio

En el nivel medio de nuestra evaluación de XSS, seguimos el enfoque habitual de comenzar con un ataque simple para verificar si hay vulnerabilidades. Sin embargo, notamos que el ataque más básico que hemos empleado anteriormente no tiene éxito en este nivel.

<div style={{textAlign: 'center'}}>
    ![Input XSS basico](/img/attacks/XSS/DOM/input-dom.png)
</div>

Este resultado sugiere que el nivel medio puede contar con medidas de seguridad más robustas en comparación con el nivel bajo. Es posible que se hayan implementado controles adicionales o filtros más estrictos para prevenir ataques XSS.

Dado que el ataque más simple no funcionó, debemos adoptar un enfoque más avanzado o explorar otras técnicas para encontrar posibles vulnerabilidades de XSS en este nivel medio. Esto puede implicar analizar más a fondo el código fuente de la aplicación, identificar posibles puntos de entrada para el XSS y probar diferentes vectores de ataque para encontrar una posible explotación.

Al examinar el código fuente relevante, notamos que se utiliza la función stripos, la cual encuentra la posición de la primera aparición de un subcadena en una cadena sin distinguir entre mayúsculas y minúsculas. Esta función se utiliza probablemente para validar o procesar el valor del parámetro proporcionado en la URL.

```php
    # Do not allow script tags
    if (stripos ($default, "<script") !== false) {
        header ("location: ?default=English");
        exit;
    }
```

Al intentar inyectar el tag **script** en la URL para ejecutar un script de XSS, nos encontramos con que la función stripos detecta la presencia de esta cadena, lo que resulta en una redirección a la URL por defecto o a una acción predefinida en el código. Esto indica que se han implementado medidas de seguridad para detectar y evitar intentos de XSS mediante la inspección del contenido de la URL.

Como hemos visto en otro ejemplo de XSS, vamos a utilizar los eventos de HTML, por lo que probamos la siguiente url

```js
English<img src=x onerror=alert("medium")>
```

Esta técnica demuestra una estrategia efectiva para mitigar vulnerabilidades de XSS al nivel medio, al utilizar funciones de PHP que examinan el contenido de las cadenas de manera sensible a las mayúsculas y minúsculas para evitar posibles ataques XSS. Sin embargo, debemos continuar evaluando otras posibles vulnerabilidades y técnicas de explotación en este nivel para garantizar la seguridad de la aplicación de manera integral.

Quedándose como en el anterior caso, pero vamos a inspeccionar el elemento por si sucedió algo.

<div style={{textAlign: 'center'}}>
    ![Input XSS basico](/img/attacks/XSS/DOM/inspect-code.png)
</div>

Podemos observar que, el código que insertamos en la URL se nos ha metido dentro del option, por lo que tenemos que cerrar el option y el select para que lo escriba fuera.

```url
English"></option></select><img src=x onerror=alert("medium")>
```

Al hacer esto observamos que la inyección de código a sido exitosa.

<div style={{textAlign: 'center'}}>
    ![Ataque medio](/img/attacks/XSS/DOM/attack-medium.png)
</div>

## Alto

En el nivel alto de nuestro análisis de XSS, al aplicar los mismos ataques que utilizamos en los niveles anteriores, notamos que el elemento select de la interfaz de usuario no reacciona a los intentos de inyección de código. Esto sugiere que las medidas de seguridad implementadas en este nivel son más estrictas y efectivas para prevenir ataques XSS.

<div style={{textAlign: 'center'}}>
    ![Input dom](/img/attacks/XSS/DOM/input-dom.png)
</div>

Al inspeccionar el código relevante en este nivel alto, podemos observar que se realiza una verificación mediante un switch para determinar si el valor proporcionado es válido.

```php
  switch ($_GET['default']) {
        case "French":
        case "English":
        case "German":
        case "Spanish":
            # ok
            break;
        default:
            header ("location: ?default=English");
            exit;
    }
```

El uso de un switch en este contexto indica que se ha implementado un mecanismo de validación más sólido para asegurarse de que solo se acepten ciertos valores predefinidos. Esto puede ser una medida de seguridad destinada a proteger la aplicación contra posibles intentos de manipulación de datos maliciosos o inesperados.

La utilización de una estructura de control como un switch para la validación es una práctica recomendada en el desarrollo de aplicaciones web, ya que permite gestionar de manera eficiente múltiples casos y garantizar que solo se acepten valores específicos que sean seguros y legítimos.

Esta implementación demuestra un enfoque proactivo hacia la seguridad de la aplicación, lo que contribuye a reducir el riesgo de vulnerabilidades como el XSS al limitar las entradas a valores validados y predefinidos. Sin embargo, es importante continuar evaluando y actualizando regularmente las medidas de seguridad para adaptarse a las nuevas amenazas y vulnerabilidades emergentes.

En las URL, las anclas se utilizan para identificar y desplazarse a secciones específicas dentro de una página web. La parte de la URL que sigue al símbolo "#" (denominada fragmento) se conoce como ancla. Es importante destacar que el fragmento de la URL, incluido el símbolo "#", no se envía al servidor cuando se realiza una solicitud HTTP.

```url
http://localhost/vulnerabilities/xss_d/?default=English#dvwa
```

Debido a que el fragmento de la URL no se envía al servidor, no puede ser filtrado o procesado por lógica del servidor. Esto significa que los caracteres o contenido presentes después del "#" no son considerados en la determinación de la respuesta del servidor. Por lo tanto, los ataques de inyección de código, como los ataques XSS, no pueden ser prevenidos o mitigados mediante la filtración del fragmento de la URL en el lado del servidor.

Al probar la anterior url nos encontramos con que se pinta en el select.

<div style={{textAlign: 'center'}}>
    ![Ataque alto inicial](/img/attacks/XSS/DOM/attack-high-initial.png)
</div>

Esto es debido a una mala programación del JavaScript que renderiza la página.

```php
if (document.location.href.indexOf("default=") >= 0) {
    var lang = document.location.href.substring(document.location.href.indexOf("default=")+8);
    document.write("<option value='" + lang + "'>" + decodeURI(lang) + "</option>");
    document.write("<option value='' disabled='disabled'>----</option>");
}
```

Que como vemos pinta tal cual llega de la URL lo que hay en default, por lo que si ejecutamos la siguiente URL.

```url
http://localhost/vulnerabilities/xss_d/?default=English#<script>alert("high")</script>
```

Conseguimos realizar el ataque.

<div style={{textAlign: 'center'}}>
    ![Ataque alto](/img/attacks/XSS/DOM/attack-high.png)
</div>

Es importante tener en cuenta esta limitación al diseñar medidas de seguridad para proteger una aplicación web. Aunque el fragmento de la URL no representa un riesgo de seguridad directo, es fundamental implementar otras medidas de seguridad, como la validación y el filtrado de la entrada del usuario, la sanitización de datos y el uso de políticas de seguridad de contenido, para protegerse contra posibles vulnerabilidades, incluidos los ataques XSS.
