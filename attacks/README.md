# Introducción

En este manual aprenderemos los conceptos básicos de seguridad web utilizando la plataforma [DVWA](https://github.com/digininja/DVWA) para practicas.

DVWA es una plataforma vulnerable escrita en PHP/MySQL separada por las diferentes vulnerabilidades que existen, para ayudar a los profesionales de la seguridad y programadores a aprender la menera de vulnerar y progeter sus aplicaciones.

## Descarga de responsabilidad

El autor no se hace responsable de los daños que se puedan causar por el mal uso de estas técnicas. Este manual esta para aprender seguridad web y mejorar nuestras técnicas de desarrollo seguras.

En ningún momento el material aquí incluido esta pensando para usos delictivos solo es información para mejorar nuestros conocimientos.
