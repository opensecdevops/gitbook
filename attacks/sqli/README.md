# SQL Injection

La Inyección SQL es una técnica de ataque que explota vulnerabilidades en la validación de entradas de las aplicaciones para ejecutar código malicioso en las bases de datos. Este ataque se produce cuando los controles de seguridad son insuficientes o cuando las variables no son adecuadamente filtradas, afectando cualquier lenguaje de programación o script involucrado.

La Inyección SQL se manifiesta cuando un atacante inserta código SQL malicioso a través de entradas no validadas. Por ejemplo, considere el parámetro "nombreUsuario" en una consulta SQL vulnerable:

```sql
SELECT * FROM usuarios WHERE nombre = '" + nombreUsuario + "';
```

Si el valor de "nombreUsuario" es simplemente "Alicia", la consulta funciona como se espera. Sin embargo, un atacante podría introducir un valor malicioso como:

```sql
Alicia'; DROP TABLE usuarios; SELECT * FROM datos WHERE nombre LIKE '%
```

Esto transformaría la consulta original en una secuencia de comandos peligrosos:

```sql
SELECT * FROM usuarios WHERE nombre = 'Alicia'; DROP TABLE usuarios; SELECT * FROM datos WHERE nombre LIKE '%';
```

Este ataque puede resultar en la exposición de registros, la eliminación de tablas y la manipulación de datos.

Para prevenir la Inyección SQL, es fundamental implementar consultas preparadas y una validación de entradas rigurosa. Estas prácticas de programación segura son esenciales para proteger las bases de datos y mantener la integridad de los sistemas.
