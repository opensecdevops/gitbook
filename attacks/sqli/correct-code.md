# Código correcto

Utilizar PDO (PHP Data Objects) es una excelente práctica para prevenir inyecciones SQL en PHP, gracias a su enfoque en consultas preparadas o "prepared statements". Este método permite separar los datos de las instrucciones SQL, evitando así que los atacantes inyecten código malicioso. Aquí tienes una explicación detallada y las buenas prácticas que debes seguir al utilizar PDO en tus aplicaciones PHP:

## Conexión a la Base de Datos con PDO

Para establecer una conexión segura con la base de datos usando PDO, se recomienda manejar excepciones para detectar errores de conexión de manera efectiva.

```php
<?php
$servername = "localhost";
$username = "your_username";
$password = "your_password";
$dbname = "your_database";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // Establece el modo de error a excepción para capturar y manejar errores adecuadamente
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
```

## Realizar consultas seguras utilizando Prepared Statements

Los *prepared statements* aseguran que los datos proporcionados por los usuarios no se evalúen como código SQL, evitando así la ejecución de inyecciones SQL.

### Consultar Datos

```php
<?php
// Preparar la consulta
$stmt = $conn->prepare("SELECT * FROM users WHERE email = :email AND password = :password");

// Asignar valores a los parámetros
$email = 'user@example.com';
$password = 'user_password';
$stmt->bindParam(':email', $email);
$stmt->bindParam(':password', $password);

// Ejecutar la consulta
$stmt->execute();

// Obtener los resultados
$result = $stmt->fetch(PDO::FETCH_ASSOC);
?>
```

### Insertar datos

```php
<?php
// Preparar la consulta
$stmt = $conn->prepare("INSERT INTO users (username, email, password) VALUES (:username, :email, :password)");

// Asignar valores a los parámetros
$username = 'new_user';
$email = 'newuser@example.com';
$password = 'new_password';
$stmt->bindParam(':username', $username);
$stmt->bindParam(':email', $email);
$stmt->bindParam(':password', $password);

// Ejecutar la consulta
$stmt->execute();
?>
```

### Actualizar datos

```php
<?php
// Preparar la consulta
$stmt = $conn->prepare("UPDATE users SET email = :email, password = :password WHERE id = :id");

// Asignar valores a los parámetros
$email = 'updateduser@example.com';
$password = 'updated_password';
$id = 1;
$stmt->bindParam(':email', $email);
$stmt->bindParam(':password', $password);
$stmt->bindParam(':id', $id);

// Ejecutar la consulta
$stmt->execute();
?>
```

### Eliminar datos

```php
<?php
// Preparar la consulta
$stmt = $conn->prepare("DELETE FROM users WHERE id = :id");

// Asignar valores a los parámetros
$id = 2;
$stmt->bindParam(':id', $id);

// Ejecutar la consulta
$stmt->execute();
?>
```

## Buenas Prácticas y Consideraciones Adicionales

- **Validación de Datos:** Además de usar consultas preparadas, valida y sanea los datos de entrada para reforzar la seguridad.
- **Uso de Contraseñas Seguras:** Para almacenar contraseñas, utiliza funciones de hashing seguras como `password_hash()` y `password_verify()` en PHP.
- **Seguridad en la Configuración:** Asegura tu configuración de PHP y la base de datos para minimizar riesgos adicionales.
- **Actualizaciones y Formación Continua:** Mantente al día con las últimas prácticas y vulnerabilidades siguiendo recursos como OWASP.

Implementar estas prácticas no solo mejora la seguridad de tus aplicaciones sino que también contribuye a un entorno de desarrollo más robusto y confiable.
