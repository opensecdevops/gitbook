# Command injection

El ataque de inyección de comandos, conocido como "Command Injection" en inglés, representa una amenaza de seguridad grave. Este tipo de vulnerabilidad permite que un atacante ejecute comandos no autorizados en un sistema operativo a través de una aplicación con fallos de seguridad. Se produce cuando una aplicación web o un programa ejecuta comandos del sistema operativo sin validar adecuadamente las entradas proporcionadas por el usuario, lo que puede comprometer la integridad y la confidencialidad del sistema.

A continuación, exploraremos cómo esta técnica de inyección puede manifestarse en diferentes niveles de seguridad.

## Bajo

En este nivel, no hay ninguna validación o filtrado de entrada del usuario. Si entramos en la sección de command injection, nos encontramos con un input para realizar pings, al que le introducimos una IP y realiza dicho ping.

<div style={{textAlign: 'center'}}>
    ![Uso normal input comandos](/img/attacks/command-injection/normal-use.png)
</div>

Es esencial reconocer que si el programador no ha implementado medidas para validar las entradas del usuario, estos pueden concatenar comandos como lo harían en una terminal. A continuación, se presentan algunas combinaciones comunes de comandos y sus acciones:

| Comandos | Acción                                                                | ejemplo                   |
| -------- | --------------------------------------------------------------------- | ------------------------- |
| \|       | La salida del primero, es la entrada del segundo                      | cat xxx \| grep yyy       |
| \|\|     | El segundo comando se ejecutará si el primera termina sin éxito       | ping 18.2.2 \|\| cat xxxx |
| &        | Ejecutar dos o mas comandos de manera simultanea                      | ls xxx & cat yyyy         |
| &&       | El segundo comando se ejecutará solo si el primero acaba con éxito    | ping 8.8.8.8 && cat ggggg |
| ;        | El segundo comando se ejecutara sin importar el resultado del primero | ls tttt; cat yyyy         |

Estas combinaciones, si no son validadas correctamente, pueden ser aprovechadas por un atacante para ejecutar comandos maliciosos, ya que el código ejecuta directamente lo que el usuario introduce.

Gracias a esto, podemos empezar a jugar con nuestro input y, si probamos cualquiera de las anteriores combinaciones, funcionará el ataque, debido a que el código ejecuta directamente lo que introduce el usuario.

<div style={{textAlign: 'center'}}>
    ![Input Command ataque básico](/img/attacks/command-injection/attack-basic.png)
</div>

Como resultado, se listan los ficheros, dejando a nuestra imaginación qué hacer en la máquina objetivo.

## Medio

Cambiamos de nivel y probamos el mismo comando con el que hemos tenido éxito en el nivel anterior, pero en este caso no nos funciona.

<div style={{textAlign: 'center'}}>
    ![Input Command Injection ataque Medio](/img/attacks/command-injection/attack-medium.png)
</div>

En este nivel, se filtran las opciones `&&` y `;` en el código de la aplicación:

```php
    $substitutions = array( '&&' => '', ';'  => '', );
```

Dado que estas opciones están bloqueadas, debemos explorar otras alternativas en las tablas de comandos disponibles. Al probar la ejecución de comandos en paralelo, logramos nuevamente la inyección con éxito.

Esto destaca la importancia de implementar múltiples capas de seguridad y no depender únicamente de la filtración de ciertos caracteres. Los desarrolladores deben adoptar un enfoque proactivo para mitigar este tipo de vulnerabilidades, utilizando técnicas como la validación estricta de entradas, el uso de listas blancas en lugar de listas negras y la implementación de mecanismos de control de acceso adecuados.

<div style={{textAlign: 'center'}}>
    ![Input Command Injection otro ataque Medio](/img/attacks/command-injection/other-attack-medium.png)
</div>

## Alto

Terminamos con el nivel alto de seguridad. En este caso, hemos probado todos los casos de la tabla anterior sin obtener un resultado válido.

<div style={{textAlign: 'center'}}>
    ![Input Command Injection ataque Alto fallido](/img/attacks/command-injection/attack-high-fail.png)
</div>

Si observamos el código, todas las opciones de los niveles anteriores están en la lista negra (blacklist):

```php
    $substitutions = array( '&'  => '', ';'  => '', '| ' => '', '-'  => '', '$'  => '', '('  => '', ')'  => '', '`'  => '', '||' => '', );
```

Sin embargo, al observar más de cerca, notamos que el operador `|` tiene un espacio después de él. Esto significa que podemos intentar ejecutar el comando sin espacio después del operador, lo que permite evadir la lista negra y lograr la inyección.

<div style={{textAlign: 'center'}}>
    ![Input Command Injection ataque Alto](/img/attacks/command-injection/attack-high.png)
</div>

En resumen, la inyección de comandos del sistema operativo es una técnica peligrosa que permite a los atacantes ejecutar comandos arbitrarios en un servidor web. Para prevenir este tipo de ataques, es importante seguir las prácticas de programación segura, como la validación adecuada de las entradas del usuario y el uso de listas blancas en lugar de listas negras para filtrar comandos y caracteres no deseados.
