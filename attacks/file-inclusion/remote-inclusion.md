# RFI

## Bajo

Volvemos a empezar y este caso vamos a inyectar un fichero remoto, como ejemplo vamos a usar google, como sabemos que carga lo que le pases por parámetros vamos a introducir la url completa y ver que sucede.

<div style={{textAlign: 'center'}}>
    ![Url para el ataque](/img/attacks/file-inclusion/RFI/attack-basic-url.png)
</div>

Como vemos nos carga google en nuestra web.

<div style={{textAlign: 'center'}}>
    ![Ataque exitoso](/img/attacks/file-inclusion/RFI/attack-basic.png)
</div>

## Medio

Cambiamos de nivel y volvemos a probar el dominio de google, dejando de funcionar

<div style={{textAlign: 'center'}}>
    ![Ataque fallido](/img/attacks/file-inclusion/RFI/attack-medium-fail.png)
</div>

Si nos fijamos en el código nos damos cuenta de que tiene un `srt_replace` como en LFI que nos impide usar http o https

```php
// Input validation 
$file = str_replace( array( "http://", "https://" ), "", $file );
```

Pero como `str_replace` no es recursivo, podemos jugar con ello cambiando la manera de escribir el protocolo

```bash
htthttp://p://google.es
```

Lo que sucederá es que se eliminara el `http://` dejando el que lo envuelve en su lugar, volviendo a funcionar el ataque.

<div style={{textAlign: 'center'}}>
    ![Ataque exitoso](/img/attacks/file-inclusion/RFI/attack-basic.png)
</div>

## Alto

En este caso no se puede resolver incluyendo una url, por lo que hay que usar otra vulnerabilidad como file upload y llamar al código desde ese fichero.
