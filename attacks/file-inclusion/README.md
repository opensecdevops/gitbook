# File Inclusion

Una vulnerabilidad de inclusión de archivos (File Inclusion) es un tipo de vulnerabilidad que, con mayor frecuencia, afecta las aplicaciones web que dependen de un tiempo de ejecución de scripts.

Este problema se produce cuando una aplicación crea una ruta de acceso al código ejecutable utilizando una variable controlada por el atacante de forma que permite al atacante controlar qué archivo se ejecuta en tiempo de ejecución.

Tenemos dos ataques posibles LFI o RFI

* Local File Inclusion (LFI) - Nos permite cargar ficheros locales.
* Remote File Inclusion (RFI)  - Nos permite cargar ficheros de manera remota.

Si entramos en la sección nos encontramos con una lista de ficheros.

<div style={{textAlign: 'center'}}>
    ![Listado de ficheros para la inclusión](/img/attacks/file-inclusion/file-inclusion.png)
</div>

Los cuales si hacemos click sobre ellos se nos cargan, ejecutando su código.

<div style={{textAlign: 'center'}}>
    ![Fichero cargado](/img/attacks/file-inclusion/load-file-inclusion.png)
</div>

Si nos fijamos en la url, vemos que es el nombre del fichero que acabamos de hacer click

<div style={{textAlign: 'center'}}>
    ![Url archivo incluye](/img/attacks/file-inclusion/url-file-inclusion.png)
</div>
