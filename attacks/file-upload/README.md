# File Upload

La carga de archivos representa un riesgo significativo para las aplicaciones. Es el primer paso en muchos ataques, ya que permite al atacante introducir código malicioso en el sistema. Una vez que se logra esto, el ataque solo necesita encontrar la forma de ejecutar el código. El uso de la carga de archivos facilita este primer paso para el atacante.

Las consecuencias de cargar archivos sin restricciones pueden ser diversas e incluyen la toma completa del sistema, sobrecarga del sistema de archivos o base de datos, reenvío de ataques a sistemas en segundo plano, ataques del lado del cliente o simplemente desfiguración. Estas consecuencias dependen de cómo la aplicación maneje el archivo cargado y, especialmente, de dónde lo almacene.

Existen dos clases de problemas asociados con la carga de archivos. El primero se refiere a los metadatos del archivo, como la ruta y el nombre del archivo. Estos datos son proporcionados por el transporte, como la codificación HTTP de varias partes. Si no se validan adecuadamente, pueden engañar a la aplicación para sobrescribir un archivo crítico o almacenar el archivo en una ubicación incorrecta.

El segundo tipo de problema se relaciona con el tamaño o contenido del archivo. Los problemas asociados aquí dependen completamente de la forma en que la aplicación utiliza el archivo. Es esencial analizar todo el proceso de manejo de archivos en la aplicación y considerar cuidadosamente qué procesos e intérpretes están involucrados para protegerse contra este tipo de ataques.

## Ataques en la Plataforma de Aplicaciones

1. **Subir archivo .gif para cambiar de tamaño**: explota la vulnerabilidad de la biblioteca de imágenes.
2. **Cargar archivo .jsp en el árbol web**: el código JSP se ejecuta como el usuario web.
3. **Subir archivos enormes**: provoca una denegación de servicio por falta de espacio en disco.
4. **Cargar archivo usando una ruta o nombre malicioso**: sobrescribe un archivo crítico en el sistema.
5. **Subir archivo que contiene datos personales**: expone información sensible a otros usuarios.
6. **Subir archivo que contiene "etiquetas"**: las etiquetas se ejecutan como parte de la inclusión en una página web.
7. **Cargar archivo .rar para ser analizado por antivirus**: se ejecuta un comando en un servidor con software antivirus vulnerable.

## Ataques en Otros Sistemas

1. **Cargar archivo .exe en el árbol web**: las víctimas descargan el ejecutable troyano.
2. **Subir archivo infectado con virus**: las máquinas de las víctimas se infectan al descargar el archivo.
3. **Cargar archivo .html que contiene secuencia de comandos**: las víctimas experimentan Cross-site Scripting (XSS).
4. **Cargar archivo .jpg que contiene un objeto Flash**: la víctima experimenta el secuestro de contenido entre sitios.
5. **Cargar archivo .rar para ser analizado por antivirus**: se ejecuta un comando en un cliente con software antivirus vulnerable.

El ejemplo proporcionado muestra diferentes niveles de seguridad (bajo, medio y alto) y cómo se pueden explotar las vulnerabilidades en cada nivel al cargar archivos maliciosos. En cada caso, se utilizan diversas técnicas, como el uso de OWASP ZAP, la concatenación de archivos y la manipulación de números mágicos, para eludir las restricciones de carga de archivos y ejecutar código malicioso en el sistema objetivo.

## Bajo

En este nivel de seguridad, al ingresar en la sección correspondiente, nos encontramos con un input que permite cargar archivos. Aprovechamos esta funcionalidad para llevar a cabo una prueba de carga de archivos maliciosos.

<div style={{textAlign: 'center'}}>
    ![Uso normal input](/img/attacks/file-upload/file-upload-low-input.png)
</div>

Creamos una shell que contiene un mensaje simple, diseñado para demostrar la capacidad de carga y ejecución de archivos maliciosos en el sistema, pero que podría programarse según nuestras necesidades.

```html
<!DOCTYPE html>
<html>
<head>
    <title>Shell DVWA</title>
</head>
<body>
    <h1>Shell low</h1>
</body>
</html>
```

Probamos a subir el archivo y vemos que se carga correctamente.

<div style={{textAlign: 'center'}}>
    ![Subida correcta fichero bajo](/img/attacks/file-upload/file-upload-low-correct-upload.png)
    Subida Correcta
</div>

Tras la carga del archivo, verificamos que la subida se realiza con éxito. Posteriormente, accedemos a la ruta donde se ha cargado el archivo y confirmamos que la ejecución de la shell es exitosa. Este proceso demuestra una grave vulnerabilidad en el sistema de carga de archivos, ya que permite la ejecución de código malicioso.

<div style={{textAlign: 'center'}}>
    ![Ejecucion Shell bajo](/img/attacks/file-upload/file-upload-low-shell.png)
    Shell nivel bajo
</div>

En resumen, en este nivel de seguridad bajo, pudimos cargar una shell maliciosa y ejecutarla exitosamente, lo que resalta la importancia de implementar medidas de seguridad más sólidas en el sistema de carga de archivos.

## Medio

En este nivel de seguridad intermedio, al intentar subir la misma shell maliciosa, notamos que no podemos completar la carga del archivo.

<div style={{textAlign: 'center'}}>
    ![Error subida archivo Medio](/img/attacks/file-upload/file-upload-medium-fail-upload.png)
</div>

Al examinar el código de la aplicación, encontramos que se realiza una comprobación del content-type del archivo para asegurarse de que sea una imagen JPEG o PNG, y que su tamaño sea inferior a 100,000 bytes.

```php
// Is it an image?
    if( ( $uploaded_type == "image/jpeg" || $uploaded_type == "image/png" ) &&
        ( $uploaded_size < 100000 ) ) {
```

Decidimos emplear OWASP ZAP para modificar el content-type de nuestro archivo y eludir así la restricción impuesta por la aplicación.

<div style={{textAlign: 'center'}}>
    ![Codigo web Medio](/img/attacks/file-upload/file-upload-medium-code.png)
</div>

En OWASP ZAP, identificamos que el content-type es `application/octet-stream`. Modificamos este valor y lo reemplazamos por uno de los content-types permitidos, como `image/jpeg` o `image/png`.

<div style={{textAlign: 'center'}}>
    ![Codigo modificado OWASP ZAP](/img/attacks/file-upload/file-upload-medium-code-modify-content-type.png)
</div>

Una vez realizado este ajuste en OWASP ZAP, procedemos con la carga del archivo y observamos que se completa exitosamente. Ahora solo nos queda ir a su ruta y verificamos que la ejecución de la shell maliciosa también se realiza sin problemas, lo que indica una vulnerabilidad significativa en el sistema de carga de archivos.

<div style={{textAlign: 'center'}}>
    ![Subida correcta fichero Medio](/img/attacks/file-upload/file-upload-medium-correct-upload.png)
    Subida correcta
</div>

<div style={{textAlign: 'center'}}>
    ![Ejecucion Shell Medio](/img/attacks/file-upload/file-upload-medium-shell.png)
    Shell media
</div>

En resumen, en este nivel de seguridad intermedio, pudimos sortear la restricción del content-type utilizando OWASP ZAP y cargar con éxito la shell maliciosa, demostrando así una vulnerabilidad importante en el sistema de carga de archivos.

## Alto

En este nivel de seguridad más elevado, al repetir el proceso realizado en los niveles anteriores, nuevamente nos encontramos con la imposibilidad de subir la shell maliciosa.

<div style={{textAlign: 'center'}}>
    ![Error subida archivo Alto](/img/attacks/file-upload/file-upload-high-fail-upload.png)
</div>

Al analizar el código de la aplicación, observamos que se realiza una verificación de la extensión del archivo y se utiliza la función getimagesize para obtener el tamaño de una imagen. Esto indica que el sistema requiere la carga de un archivo que se considere una "imagen".

```php
if( ( strtolower( $uploaded_ext ) == "jpg" || strtolower( $uploaded_ext ) == "jpeg" || 
strtolower( $uploaded_ext ) == "png" ) && ( $uploaded_size < 100000 ) && getimagesize( $uploaded_tmp ) ) {
```

Por lo tanto, tenemos que subir una "imagen" sí o sí.
Dadas las restricciones impuestas por la aplicación, nos vemos obligados a buscar alternativas para cargar la shell maliciosa. Tenemos dos opciones disponibles:

1. Utilizar el comando 'cat' para concatenar dos archivos (una imagen y una shell).
2. Emplear '[magic numbers](https://es.wikipedia.org/wiki/N%C3%BAmero_m%C3%A1gico_(inform%C3%A1tica))' ([lista](https://www.garykessler.net/library/file_sigs.html)) para engañar al sistema.

### cat

Para la opción del comando 'cat', procedemos a crear una imagen de un píxel con el editor de imágenes de nuestra elección o podemos emplear cualquier imagen disponible. Luego, concatenamos esta imagen con la shell maliciosa mediante el siguiente comando:

```bash
cat shellimagebase.png shell.php > shellfinal.png
```

Este comando nos genera una nueva imagen que contiene la combinación de la imagen original y la shell maliciosa, la cual vamos a subir al sistema.

<div style={{textAlign: 'center'}}>
    ![Subida correcta archivo cat Alto](/img/attacks/file-upload/file-upload-high-correct-upload.png)
</div>

Tras subir la imagen generada, verificamos que la carga se realiza correctamente, lo que nos permite acceder a ella a través de la ruta correspondiente. Al abrir la imagen, podemos observar su contenido visual sin problemas.

<div style={{textAlign: 'center'}}>
    ![Imagen subida visible](/img/attacks/file-upload/file-upload-high-image-created.png)
</div>

Sin embargo, si deseamos ejecutar el código malicioso contenido en la shell, es necesario combinar este ataque con File Inclusion. Para ello, nos dirigimos a la sección de File Inclusion y cargamos el archivo. Como resultado, la imagen no se renderiza, pero el código de la shell se ejecuta con éxito, lo que pone de manifiesto la eficacia de este tipo de ataque.

<div style={{textAlign: 'center'}}>
    ![Ejecucion Shell File Inclusion Alto](/img/attacks/file-upload/file-upload-high-file-inclusion-shell.png)
    Shell alta mediante cat y File Inclusion
</div>

### Magic Number

Para la opción de 'magic numbers', agregamos el código hexadecimal correspondiente a los archivos PNG al comienzo del archivo de nuestra shell. Primero, abrimos la shell con un [editor hexadecimal](https://hexed.it/).

<div style={{textAlign: 'center'}}>
    ![Editor hexadecimal codigo Shell](/img/attacks/file-upload/file-upload-magic-number-hexadecimal.png)
</div>

Copiamos el 'magic number' para los archivos PNG:

```hex
89 50 4E 47 0D 0A 1A 0A
```

Introducimos el 'magic number' al comienzo del archivo de nuestra shell, de modo que el archivo se identifique como un archivo PNG.

<div style={{textAlign: 'center'}}>
    ![Insert codigo png en Shell](/img/attacks/file-upload/file-upload-magic-number-insert-shell.png)
</div>

Así, el archivo quedará de la siguiente manera, con el comienzo del archivo identificado como PNG.

<div style={{textAlign: 'center'}}>
    ![Editor hexadecimal codigo Shell modificado](/img/attacks/file-upload/file-upload-magic-number-hexadecimal-shell.png)
</div>

Tras modificar el archivo de la shell, procedemos a subirlo al sistema. Una vez que el archivo se carga correctamente, verificamos su funcionamiento accediendo a la ruta correspondiente.

<div style={{textAlign: 'center'}}>
    ![Subida correcta archivo magic number Alto](/img/attacks/file-upload/file-upload-magic-number-correct-upload.png)
    Subida correcta
</div>

En este caso, la imagen no se carga visualmente en el sistema, ya que solo está presente la cabecera de la imagen para engañar al sistema.

<div style={{textAlign: 'center'}}>
    ![Shell subida pero no visible](/img/attacks/file-upload/file-upload-magic-number-no-visible-shell.png)
    Shell subida pero no visible
</div>

Sin embargo, si realizamos la inclusión del archivo mediante File Inclusion, la shell se carga y funciona correctamente, lo que pone de manifiesto la eficacia de este tipo de ataque en combinación con técnicas adicionales.

<div style={{textAlign: 'center'}}>
    ![Ejecucion Shell File Inclusion Alto](/img/attacks/file-upload/file-upload-magic-number-file-inclusion-shell.png)
    Shell cargada mediante File Inclusion
</div>
