# Instalación

Para proceder a la instalacion de Harbor en nuestra plataforma como con otras herramientas primero necesitamos clonar el repositorio del mismo, para asi poder ver los valores que deseamos utilizar en nuestro despliegue.

## Repo

[https://github.com/goharbor/harbor-helm.git](https://github.com/goharbor/harbor-helm.git)

Hacemos 
```bash
git clone https://github.com/goharbor/harbor-helm.git
```

Vamos al directorio recien clonado.
```bash
cd harbor-helm
```
Y procederemos a copiar o modificar el values.yaml.
En el values.yaml debemos tener los siguientes parametros.
```yaml
expose:
  type: ingress
  tls:
    enabled: true
    secret:
      secretName: "SECRET_CERT"
    certSource: secret
  ingress:
    hosts:
      core: "harbor.yourdomain.com"
  # Set the ingressClassName on the ingress record
      className: "appsec-nginx"
  annotations:
    k8s.io/appsec-nginx: appsec-nginx
    nginx.ingress.kubernetes.io/proxy-body-size: "0"

trivy:
  enabled: true
notary:
  enabled: false
externalURL: "https://harbor.yourdomain.com"
harborAdminPassword: admin
persistence:
  persistentVolumeClaim:
    registry:
      size: 20Gi
```
y luego procedemos a la instalación con el helm

```bash
helm repo add harbor https://helm.goharbor.io 

helm upgrade --install -n harbor harbor -f values.yaml harbor/harbor --create-namespace
```

Del anterior comando podemos notar los siguientes detalle:

1. Se usa 'helm upgrade --install' porque si no existe los instale y si existe lo actualice.
2. Se usa '--create-namespace' para que cree el namespace en Kuberentes si no esta creado.
3. Se usa '-n harbor' para indicar el namespace donde queremos instalar nuestra aplicacion.

:::warning
**El helm tiene un bug y tienes que editar el ingress de harbor y agregar ingressClassName: appsec-nginx debajo de spec para que el ingress coja IP**
:::
