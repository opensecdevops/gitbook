# Instalación

## Docker-Compose

Glitchtip dispone de imagenes de docker ya preparadas por lo que solo tenemos que realizar unas pequeñas configuraciones.
Lo primero que vamos a realizar es crear un fichero `.env` con los datos de configuración de la base de datos.

```vim title=".env"
POSTGRES_DB=glitchtip
POSTGRES_USER=glitchtipOSDO
POSTGRES_PASS=glitchtipPASS
```

Creamos la red para conectar el defectdojo con el traefik

```bash
docker network create glitchtip
```

Creamos el `SECRET_KEY` que necesita la aplicación para funcionar y los sustitumos en las variables de entorno del `docker-compose.yml`

```bash
openssl rand -hex 32
```

Tenemos que cambiar en el label de traefik del Host y la variable `GLITCHTIP_DOMAIN` de `example.com` por el dominio que queramos.

:::warning
En la configuración actual el sistena **no** envia emails.

Para que envie emails hay que [configurar](https://glitchtip.com/documentation/install#configuration) nuestro servidor smtp, [Mailgun](https://www.mailgun.com/) o [SendGrid](https://sendgrid.com/)
:::

```yml title="docker-compose.yml"
version: "3.8"
x-environment:
  &default-environment
  DATABASE_URL: postgres://postgres:postgres@postgres:5432/postgres
  SECRET_KEY: CREATE_NET_SECRET_KEY
  PORT: 8000
  EMAIL_URL: consolemail://
  GLITCHTIP_DOMAIN: https://glitchtip.example.com # Change this to your domain
  DEFAULT_FROM_EMAIL: info@example.com # Change this to your email
  CELERY_WORKER_MAX_TASKS_PER_CHILD: "10000"
  ENABLE_USER_REGISTRATION: "false"

x-depends_on:
  &default-depends_on
  - postgres
  - redis

services:
  postgres:
    image: postgres:15
    container_name: postgres-glitchtip
    environment:
      POSTGRES_HOST_AUTH_METHOD: "trust"  # Consider removing this and setting a password
    restart: unless-stopped
    volumes:
      - pg-data:/var/lib/postgresql/data
    networks:
      - glitchtip
  redis:
    image: redis
    container_name: redis-glitchtip
    restart: unless-stopped
    networks:
      - glitchtip
  web:
    image: glitchtip/glitchtip@sha256:4b67df9b1ee43f02ab7167caf84f9df24377e4de1cffc748c37932e757ca1049
    container_name: web-glitchtip
    depends_on: *default-depends_on
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.glitchtip.rule=Host(`glitchtip.example.com`)"
      - "traefik.http.routers.glitchtip.entrypoints=websecure"
      - "traefik.http.routers.glitchtip.tls=true"
      - "traefik.http.routers.glitchtip.tls.certresolver=le"
      - "traefik.http.services.glitchtip-service.loadbalancer.server.port=8000"
    environment: *default-environment
    restart: unless-stopped
    volumes:
      - uploads:/code/uploads
    networks:
      - glitchtip
  worker:
    image: glitchtip/glitchtip@sha256:4b67df9b1ee43f02ab7167caf84f9df24377e4de1cffc748c37932e757ca1049
    command: ./bin/run-celery-with-beat.sh
    container_name: worker-glitchtip
    depends_on: *default-depends_on
    environment: *default-environment
    restart: unless-stopped
    volumes:
      - uploads:/code/uploads
    networks:
      - glitchtip
  migrate:
    container_name: migrate-glitchtip
    image: glitchtip/glitchtip@sha256:4b67df9b1ee43f02ab7167caf84f9df24377e4de1cffc748c37932e757ca1049
    depends_on: *default-depends_on
    command: "./manage.py migrate"
    environment: *default-environment
    networks:
      - glitchtip

volumes:
  pg-data:
  uploads:

networks:
  glitchtip:
    external: true
```

:::tip
Antes de arrancarlo acordaros de añadir la red en traefik
:::

Arrancamos los contenedores.

```bash
docker-compose up -d
```
