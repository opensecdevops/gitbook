# GlitchTip

GlitchTip es una herramienta de monitorización de errores que te ayuda a identificar y corregir errores en tu código durante la ejecución de un proyecto. Es una herramienta esencial para los desarrolladores de software que buscan mejorar la calidad y la confiabilidad de su software. Algunas de sus principales características son:

- Seguimiento de errores: GlitchTip te proporciona información detallada sobre cada error, incluyendo la ubicación del error, el tipo de error y la pila de llamadas.
- Monitoreo del rendimiento: GlitchTip proporciona un monitorie de donde la aplicación se vuelve lenta haciendo un tracking del performance.
- Monitoreo del tiempo de actividad: GlitchTip puede hacer ping a su sitio y advertirle cuando no responde, si GlitchTip no recibe su ping, le enviará una alerta por correo electrónico o webhook.
- SDK de Sentry: GlitchTip usa el SDK del cliente Sentry que es OpenSource por lo que si ya usa Sentry en sus proyectos, solo tiene que cambiar la url donde recibir los reportes.
