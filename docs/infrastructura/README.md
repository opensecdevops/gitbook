# Infastructura

Para configurar la infraestructura necesaria con el fin de llevar a cabo el ciclo completo o solo una parte, se realizará mediante el uso de contenedores Docker. Exploraremos dos métodos para configurar dichos contenedores, mediante Docker Compose o Kubernetes.

## Docker-compose

Docker-compose es una herramienta que permite definir y ejecutar aplicaciones multi-contenedor con Docker. Utiliza un archivo YAML para configurar los servicios, las redes y los volúmenes necesarios para cada contenedor de la aplicación.

### Ventajas de Docker-compose

1. **Simplicidad:** Docker-compose simplifica la gestión de aplicaciones multi-contenedor al definir todos los servicios en un solo archivo YAML.
2. **Entorno de desarrollo aislado:** Permite crear entornos de desarrollo aislados con todas las dependencias necesarias para ejecutar una aplicación.
3. **Rápido despliegue:** Facilita el despliegue rápido de aplicaciones en cualquier entorno compatible con Docker.
4. **Flexibilidad:** Es fácil de configurar y permite la personalización de cada contenedor según las necesidades del proyecto.

### Inconvenientes de Docker-compose

1. **Limitado a un solo Host:** Está diseñado para desplegar aplicaciones en un único host, lo que limita su capacidad para implementaciones más complejas.
2. **No escalabilidad automática:** No ofrece herramientas integradas para escalar automáticamente los contenedores en respuesta a la demanda.
3. **Gestión manual:** Requiere una gestión manual para operaciones avanzadas, como el balanceo de carga o la alta disponibilidad.
4. **Dependiente de Docker:** Su funcionalidad está ligada a Docker, lo que puede limitar las opciones en entornos donde Docker no es la mejor opción.

## Kubernetes (K8s)

Kubernetes es una plataforma de código abierto diseñada para automatizar la implementación, el escalado y la gestión de aplicaciones en contenedores. Proporciona un entorno de orquestación de contenedores altamente escalable y flexible.

### Ventajas de Kubernetes

1. **Escalabilidad automática:** Kubernetes ofrece escalabilidad automática, lo que permite aumentar o disminuir el número de réplicas de un contenedor según la carga de trabajo.
2. **Alta disponibilidad:** Proporciona características para garantizar la alta disponibilidad de las aplicaciones, incluyendo la distribución de cargas de trabajo y la tolerancia a fallos.
3. **Orquestación completa:** Ofrece una orquestación completa de contenedores, incluyendo la gestión de redes, almacenamiento, monitoreo y balanceo de carga.
4. **Multi-cloud y multi-entorno:** Es compatible con entornos multi-nube y multi-entorno, lo que permite la portabilidad de las aplicaciones entre diferentes proveedores de nube y entornos locales.

### Inconvenientes de Kubernetes

1. **Curva de aprendizaje:** Requiere tiempo y esfuerzo para familiarizarse con los conceptos y la arquitectura de Kubernetes.
2. **Complejidad:** Puede ser complejo de configurar y mantener, especialmente para aplicaciones más simples que no requieren todas las características de Kubernetes.
3. **Infraestructura necesaria:** Requiere una infraestructura adecuada para su implementación, incluyendo servidores y recursos de red.
4. **Recursos de Hardware:** Puede consumir una cantidad significativa de recursos de hardware, especialmente en despliegues a gran escala.

## Comparativa entre Docker-compose y Kubernetes

### Escalabilidad

- **Docker-compose:** Está diseñado para desplegar aplicaciones en un único host, lo que limita su capacidad para escalar automáticamente los contenedores.
- **Kubernetes:** Ofrece escalabilidad automática, permitiendo aumentar o disminuir el número de réplicas de un contenedor según la carga de trabajo, lo que lo hace ideal para aplicaciones altamente escalables.

### Complejidad y Curva de aprendizaje

- **Docker-compose:** Es fácil de aprender y usar, ya que simplifica la gestión de aplicaciones multi-contenedor con un archivo YAML.
- **Kubernetes:** Tiene una curva de aprendizaje más pronunciada debido a su arquitectura y a la cantidad de conceptos que involucra, lo que requiere un mayor tiempo y esfuerzo para dominarlo completamente.

### Orquestación y Gestión de Contenedores

- **Docker-compose:** Proporciona una gestión básica de contenedores y servicios, adecuada para aplicaciones simples o de desarrollo.
- **Kubernetes:** Ofrece una orquestación completa de contenedores, incluyendo la gestión de redes, almacenamiento, monitoreo y balanceo de carga, lo que lo hace más adecuado para aplicaciones complejas y de producción.

### Portabilidad y Multi-entorno

- **Docker-compose:** Es más portátil y se puede ejecutar en cualquier entorno compatible con Docker, pero está limitado a un solo host.
- **Kubernetes:** Es compatible con entornos multi-nube y multi-entorno, lo que permite la portabilidad de las aplicaciones entre diferentes proveedores de nube y entornos locales, proporcionando una mayor flexibilidad.

### Uso de Recursos

- **Docker-compose:** Es más ligero en cuanto a recursos, ya que está diseñado para desplegar aplicaciones en un solo host.
- **Kubernetes:** Puede consumir una cantidad significativa de recursos de hardware, especialmente en despliegues a gran escala, debido a su arquitectura distribuida y sus características avanzadas de gestión de contenedores.

En resumen, Docker-compose es más adecuado para entornos de desarrollo y pruebas o aplicaciones simples, mientras que Kubernetes es más adecuado para entornos de producción y aplicaciones complejas que requieren escalabilidad, alta disponibilidad y características avanzadas de gestión de contenedores. La elección entre ambos depende de los requisitos específicos del proyecto y del nivel de complejidad deseado para la infraestructura de contenedores.

:::warning
No todos los softwares están disponibles para ambos tipos de infraestructura.

Si el software está disponible solo para una arquitectura, se mostrará un aviso durante la instalación del mismo.
:::
