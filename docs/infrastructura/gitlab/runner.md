---
title: Runner
---

# Runner

Un Runner de GitLab es una parte fundamental del sistema de CI/CD (Integración Continua y Entrega Continua) de GitLab. Funciona como una entidad que se encarga de ejecutar trabajos específicos definidos en los archivos `.gitlab-ci.yml`. Los Runners permiten la automatización de tareas, pruebas y despliegues en un entorno de desarrollo, todo ello gestionado a través de GitLab. Aquí tienes algunas de sus características principales:

1. **Ejecución de Trabajos:** Los Runners ejecutan los trabajos definidos en los archivos `.gitlab-ci.yml`. Estos trabajos pueden incluir tareas como la construcción de código, pruebas unitarias, análisis estático de código y despliegues.
2. **Multiplataforma:** Los Runners son versátiles y pueden ejecutarse en una variedad de sistemas operativos y arquitecturas, lo que permite la compatibilidad con diferentes tipos de proyectos y entornos.
3. **Escalabilidad:** Puedes configurar múltiples Runners para un solo proyecto o incluso para varios proyectos, lo que permite escalar y distribuir la carga de trabajo según sea necesario.
4. **Docker y Contenedores:** Los Runners de GitLab son compatibles con Docker, lo que facilita la creación de entornos de prueba y despliegues de aplicaciones en contenedores.
5. **Seguridad:** GitLab ofrece opciones avanzadas de seguridad para los Runners, lo que incluye autenticación y autorización seguras para garantizar que solo los usuarios autorizados puedan ejecutar trabajos en los Runners.
6. **Paralelismo:** Los Runners pueden ejecutar trabajos en paralelo, lo que acelera el proceso de CI/CD al distribuir las tareas en varios Runners si es necesario.
7. **Personalización:** Los Runners pueden ser configurados y personalizados para satisfacer las necesidades específicas de tu proyecto, lo que te permite definir entornos, variables de entorno y requisitos de ejecución personalizados.
8. **Monitorización y Registro:** GitLab proporciona herramientas de monitorización y registro para que puedas rastrear el estado y el rendimiento de tus Runners, lo que facilita la detección y solución de problemas.
9. **Escenarios de Despliegue Complejos:** Los Runners permiten la automatización de flujos de trabajo complejos de CI/CD que abarcan múltiples etapas, desde la construcción hasta las pruebas y el despliegue en diferentes entornos.

## Instalación

Para proceder a la instalacion del runner de Gitlab en nuestra plataforma como con otras herramientas primero necesitamos revisar el values yaml en el siguiente repo [https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml), para asi poder ver los valores que deseamos utilizar en nuestro despliegue.

Luego procedemos a crear uno propio o copiar el anterior.

Debemos agregar al yaml el token que obtenemos de gitlab para registrar el runner en nuestro cluster

```yaml
runnerRegistrationToken: "glrt-FQX6FNEB6_vNUQkxnQFt"
```

Tambien procedemos a agregar las siguientes configuraciones para poder tener la cache y el docker in docker habilitados

```yaml
config: |
    [[runners]]
      [runners.kubernetes]
        namespace = "namespace del gitlab-runner"
        image = "ubuntu:latest"
        [[runners.kubernetes.volumes.host_path]]
          name = "docker-socket"
          mount_path = "/var/run/docker.sock"
          read_only = false
          host_path = "/var/run/docker.sock"
        [runners.cache]
        Type = "s3"
        Shared = true
        [runners.cache.s3]
          ServerAddress = "S3_ADDRESS"
          AccessKey = "ACCESSKEY"
          SecretKey = "SECRETKEY"
          BucketName = "BUCKETNAME"
          BucketLocation = "BUCKET_LOCATION"
          Insecure = false
```

Ahora agregamo el repo de helm

```bash
helm repo add gitlab https://charts.gitlab.io
```

y luego procedemos a instalar con el helm

## Helm

```bash
helm upgrade --install -n  gitlab-runner gitlab-runner -f values.yaml gitlab/gitlab-runner --create-namespace
```
