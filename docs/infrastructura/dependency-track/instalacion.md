# Instalación

## Docker-Compose

Dependecy-track dispone de imagenes de docker ya preparadas por lo que solo tenemos que realizar unas pequeñas configuraciones.
Lo primero que vamos a realizar es crear un fichero `.env` con los datos de configuración de la base de datos.

```vim title=".env"
USERDB=dtrackOSDO
PASSWORDDB=dtrackPassword
DTRACKDB=dtrack
```

Creamos la red para conectar el dependency-track con el traefik

```bash
docker network create dtrack
```

A continuación creamos el fichero `docker-compose.yml` en el vemos 3 contenedores

- Base de datos en postgres
- API
- Frontal

La base de datos si es para probarlo no es necesario, la propia api tiene su base de datos interna, si quitamos las lienas de `ALPINE_DATABASE`.

Tenemos que cambiar en el label de traefik del Host `example.com` por el dominio que queramos.

```yml title="docker-compose.yml" showLineNumbers
services:
  dtrack-postgres:
    image: postgres@sha256:f1314058032e52cce689f2daf3fffe7c136775e3fdd1af3fb36ae5cdc61c7891
    container_name: dtrack-postgres
    environment:
      - POSTGRES_USER=${USERDB}
      - POSTGRES_PASSWORD=${PASSWORDDB}
      - POSTGRES_DB=${DTRACKDB}
    volumes:
      - postgres_data:/var/lib/postgresql/data
    restart: unless-stopped
    networks:
      - dtrack
    labels:
      - "traefik.enable=false"

  dtrack-apiserver:
    image: dependencytrack/apiserver@sha256:bffd457f60dd4aed9a005d20b2a42b7307930518c2a081d1c28baa2c319f391d
    container_name: dtrack-api
    environment:
      - ALPINE_DATABASE_MODE=external
      - ALPINE_DATABASE_URL=jdbc:postgresql://dtrack-postgres:5432/${DTRACKDB}
      - ALPINE_DATABASE_DRIVER=org.postgresql.Driver
      - ALPINE_DATABASE_USERNAME=${USERDB}
      - ALPINE_DATABASE_PASSWORD=${PASSWORDDB}
    deploy:
      resources:
        limits:
          memory: 12288m
        reservations:
          memory: 8192m
      restart_policy:
        condition: on-failure
    volumes:
      - 'dependency-track:/data'
    restart: unless-stopped
    depends_on:
      - dtrack-postgres
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.dtrackapi.rule=Host(`dtrack-api.example.com`)"
      - "traefik.http.routers.dtrackapi.entrypoints=websecure"
      - "traefik.http.routers.dtrackapi.tls=true"
      - "traefik.http.routers.dtrackapi.tls.certresolver=le"
    networks:
      - dtrack

  dtrack-frontend:
    image: dependencytrack/frontend@sha256:63d6a6cc9f4cab15a056d4ec1ba9f8a87203415e8f1f73741fadee7f93bc191e
    container_name: dtrack
    depends_on:
      - dtrack-apiserver
    environment:
      # The base URL of the API server.
      # NOTE:
      #   * This URL must be reachable by the browsers of your users.
      #   * The frontend container itself does NOT communicate with the API server directly, it just serves static files.
      #   * When deploying to dedicated servers, please use the external IP or domain of the API server.
      - API_BASE_URL=https://dtrack-api.example.com
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.dtrack.rule=Host(`dtrack.example.com`)"
      - "traefik.http.routers.dtrack.entrypoints=websecure"
      - "traefik.http.routers.dtrack.tls=true"
      - "traefik.http.routers.dtrack.tls.certresolver=le"
    restart: unless-stopped
    networks:
      - dtrack

networks:
  dtrack:
    external: true

volumes:
  postgres_data:
  dependency-track:
```

:::tip
Antes de arrancarlo acordaros de añadir la red en traefik

Necesita por lo menos 4G de ram en el host o el contenedor no arrancara.
:::

Arrancamos los contenedores. Esto puede llevar algún tiempo, mientras el sistema java arranca.

```bash
docker-compose up -d
```

## Kubernetes

Para proceder a la instalacion de DependencyTrack en nuestra plataforma como con otras herramientas primero necesitamos clonar el repositorio del mismo, para asi poder ver los valores que deseamos utilizar en nuestro despliegue.

[https://github.com/DependencyTrack/dependency-track](https://github.com/DependencyTrack/dependency-track)

Clonamos el repositorio

```bash
git clone https://github.com/DependencyTrack/dependency-track
```

para poder ubicar el chart de helm y validar las configuraiones que queremos realizar.
Luego procedemos a agregar el chart en nuestro Kubernetes con

```bash
helm repo add evryfs-oss https://evryfs.github.io/helm-charts/
```

Aqui tenemos dos opciones, o copiamos y modificamos el values.yaml que podemos conseguir en la siguietne [ruta](https://github.com/evryfs/helm-charts/tree/master/charts/dependency-track) o creamos uno nuevo.
Si lo clonamos vamos al directorio donde se encuentra el values.yaml.

```bash
cd charts/dependency-track/
```

En este archivo debemos tener los siguientes parametros:

```yaml
frontend:
  enabled: true
  annotations: {}
  replicaCount: 2
  image:
    repository: dependencytrack/frontend
    tag: 4.6.1
    pullPolicy: IfNotPresent
  env:
    - name: API_BASE_URL
      value: "https://dtrack.yourdomain.com"
# -- configuration of ingress
ingress:
  enabled: true
  tls:
    enabled: true
    secretName: "osdo-certs"
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
    ## allow large bom.xml uploads:
    # nginx.ingress.kubernetes.io/proxy-body-size: 10m
  host: dtrack.opensecdevops.com
  ingressClassName: appsec-nginx
```

luego procedemos a instalar con helm.

```bash
helm upgrade --install dependency-track evryfs-oss/dependency-track -f values.yaml -n dependency-track --create-namespace
```

Del anterior comando podemos notar los siguientes detalle:

1. Se usa 'helm upgrade --install' porque si no existe los instale y si existe lo actualice.
2. Se usa '--create-namespace' para que cree el namespace en Kuberentes si no esta creado.
3. Se usa '-n dependency-track' para indicar el namespace donde queremos instalar nuestra aplicacion.
