# Instalación

Para proceder a la instalacion de Sonarqube en nuestra plataforma como con otras herramientas primero necesitamos clonar el repositorio del mismo, para asi poder ver los valores que deseamos utilizar en nuestro despliegue.

## Repo

[https://github.com/SonarSource/helm-chart-sonarqube.git](https://github.com/SonarSource/helm-chart-sonarqube.git)

Agregamos el repositorio de helm y actualizamos

```bash
helm repo add sonarqube https://SonarSource.github.io/helm-chart-sonarqube
helm repo update
```

Podemos clonarnos el git mencianado anteriormente y copoar el values.yaml que se encuentra en la siguiente ruta.

```bash
cd helm-chart-sonarqube/charts/sonarqube/
```

Y procederemos a copiar o modificar el values.yaml.
En el values.yaml debemos tener los siguientes parametros.

```yaml
ingress:
    enabled: true
    hosts:
      - name: sonar.yourdomain.com
        path: "/"
    # This property allows for reports up to a certain size to be uploaded to SonarQube
    annotations:
      kubernetes.io/ingress.class: appsec-nginx
      nginx.ingress.kubernetes.io/proxy-body-size: "64m"
  # Set the ingressClassName on the ingress record
    ingressClassName: appsec-nginx
    tls: 
  # Secrets must be manually created in the namespace. To generate a self-signed certificate (and private key) and then create the secret in the cluster please refer to official documentation available at https://kubernetes.github.io/ingress-nginx/user-guide/tls/#tls-secrets
    - secretName: SECRET_CERT
  #   hosts:
  #     - chart-example.local
prometheusExporter:
  enabled: false
  config:
    rules:
      - pattern: ".*"
postgresql:
  # Enable to deploy the bitnami PostgreSQL chart
  enabled: true
  persistence:
    enabled: true
    accessMode: ReadWriteOnce
    size: 10Gi
```

procedemos a la instalación con el helm

## Helm

```bash
helm upgrade --install -n sonar sonarqube -f values.yaml sonarqube/sonarqube --create-namespace
```

Del anterior comando podemos notar los siguientes detalle:

1. Se usa 'helm upgrade --install' porque si no existe los instale y si existe lo actualice.
2. Se usa '--create-namespace' para que cree el namespace en Kuberentes si no esta creado.
3. Se usa '-n sonar' para indicar el namespace donde queremos instalar nuestra aplicacion.