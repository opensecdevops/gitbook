import DocCardList from '@theme/DocCardList';

# GitOps

GitOps ofrece beneficios significativos en términos de seguridad dentro del contexto de DevSecOps, combinando las prácticas de desarrollo, operaciones y seguridad en un enfoque integrado. Algunos puntos clave sobre cómo GitOps contribuye a la seguridad en DevSecOps son:

- **Visibilidad y Rastreabilidad:**
  - **Registro de Cambios:** Almacenar todas las configuraciones y definiciones de infraestructura en repositorios Git proporciona una trazabilidad completa de todos los cambios realizados en el entorno.
  - **Auditoría Mejorada:** Facilita una mejor auditoría al registrar quién, cuándo y qué cambios se han realizado en el sistema.

- **Control de Versiones y Revisión:**
  - **Versionamiento Estructurado:** GitOps permite un control de versiones detallado para todos los cambios en la configuración, lo que facilita las revisiones y la identificación de discrepancias.
  - **Reversiones Seguras:** La capacidad de realizar rollbacks controlados a versiones anteriores reduce los riesgos asociados con cambios defectuosos o problemas de seguridad.
- **Automatización Segura:**
  - **Implementaciones Controladas:** La automatización a través de GitOps garantiza que las implementaciones sigan las políticas de seguridad y las prácticas de aprobación establecidas.
  - **Ejecución Declarativa:** La ejecución de cambios de manera declarativa desde repositorios Git evita cambios inesperados o no autorizados en la infraestructura.

- **Gestión Centralizada:**
  - **Gestión de Acceso y Permisos:** Git se convierte en el punto central para gestionar y controlar el acceso, lo que facilita la gestión de permisos y la seguridad de los datos sensibles.
  - **Consistencia y Estándares:** La gestión centralizada permite aplicar y hacer cumplir políticas de seguridad y estándares en toda la infraestructura y las aplicaciones.

- **Seguridad en la Cadena de Suministro de Software:**
  - **Validación de Cambios:** GitOps permite realizar validaciones de seguridad automáticas antes de aplicar los cambios, garantizando que solo los cambios autorizados y seguros se implementen.
  - **Integración con Herramientas de Seguridad:** Facilita la integración de herramientas de seguridad en el flujo de trabajo, permitiendo análisis estáticos y dinámicos del código y la infraestructura.
- **Resiliencia y Recuperación:**
  - **Capacidad de Recuperación:** En caso de incidentes de seguridad, la capacidad de realizar rollbacks rápidos a versiones anteriores garantiza una recuperación más rápida y segura.
  - **Backups y Restauración:** GitOps permite la gestión de backups y la restauración de configuraciones críticas para mejorar la resiliencia del sistema.
GitOps, al integrarse en DevSecOps, mejora la seguridad al ofrecer una gestión más segura, rastreable y controlada de la infraestructura y las aplicaciones, contribuyendo así a la creación de un entorno más seguro y robusto.

## Ejemplo de un manifiesto

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: database-deployment
  namespace: vulnapp
  labels:
    app: database
spec:
  replicas: 1
  selector:
    matchLabels:
      app: database
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: database
    spec:
      containers:
        - env:
            - name: MYSQL_DATABASE
              value: docker
            - name: MYSQL_PASSWORD
              value: docker
            - name: MYSQL_ROOT_PASSWORD
              value: docker
            - name: MYSQL_USER
              value: docker
          image: mysql:8
          name: vulnerable-app-db
          resources: {}
          volumeMounts:
            - mountPath: /docker-entrypoint-initdb.d
              name: database-claim0
            - mountPath: /var/lib/mysql
              name: database-claim1
      restartPolicy: Always
      volumes:
        - name: database-claim0
          configMap:
            name: docker-entrypoint
        - name: database-claim1
          persistentVolumeClaim:
            claimName: database-claim1
status: {}

```
## Pasos para configurar en ArgoCD

1. Primero debemos agregar el repositorio del cual queremos extraer los manifiestos, para ello en la interfaz de ArgoCD vamos a **Settings** y alli le damos al boton **Connect new Repo**.
2. Elegimos metodo de conexion, rellenamos los datos de dicho metodo y guardamos.
3. Luego vamos a **Applications** y aqui le damos al boton **new app**.
4. Nos aparecera para rellenar informacion sobre la aplicaión que queremos desplegar, debemos tomar en cuenta que la seccion de **Source** es donde elegiremos el repositorio anteriormente conectado y seccion **Destination** colocamos el namespace y el cluster al cual queremos desplegar.

Con eso ya el ArgoCD se conecta a nuestro repositorio, extrae los manifiestos del codigo fuente, y procede a desplegarlos en lugar indicado por nosotros, ya con esto podemos observar como se va sincronizando el despliegue.

<DocCardList />
