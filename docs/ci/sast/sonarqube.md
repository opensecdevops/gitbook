# SonarQube

SonarQube es una plataforma de código abierto líder en el mercado que se utiliza para evaluar y mejorar la calidad del código fuente de aplicaciones y proyectos de software. Ofrece una solución integral para identificar y corregir problemas de código, garantizar la conformidad con las mejores prácticas de codificación y mejorar la seguridad y la confiabilidad del software.

## Características principales

**Análisis estático del código:** SonarQube realiza análisis estáticos exhaustivos del código fuente en busca de vulnerabilidades, errores y problemas de calidad sin necesidad de ejecutar la aplicación.

**Detección de vulnerabilidades:** Identifica vulnerabilidades comunes de seguridad, como inyecciones SQL, vulnerabilidades de seguridad de datos, problemas de autenticación y autorización, entre otros.

**Medición de la complejidad:** Evalúa la complejidad del código, identificando áreas que pueden ser difíciles de mantener y propensas a errores.

**Integración continua y entrega continua (CI/CD):** Puede integrarse fácilmente en pipelines de CI/CD, permitiendo la evaluación automática del código en cada etapa del ciclo de desarrollo.

**Calidad del código:** Proporciona puntuaciones y métricas de calidad del código, lo que ayuda a los equipos a mantener un alto estándar de calidad.

**Soporte para múltiples lenguajes:** SonarQube es compatible con una amplia gama de lenguajes de programación, incluidos Java, C#, JavaScript, Python, Ruby y más.

**Gestión de deudas técnicas:** Identifica y prioriza la deuda técnica, lo que permite a los equipos abordar problemas críticos primero.

**Paneles de control personalizables:** Ofrece paneles de control personalizables y visualizaciones de datos para un seguimiento y una toma de decisiones efectivos.

**Escaneos programados:** Permite la programación de escaneos regulares para mantener la calidad del código a lo largo del tiempo.

**Integración con IDEs:** Proporciona extensiones para IDEs populares, lo que permite a los desarrolladores identificar y solucionar problemas de código mientras escriben.

**Seguridad y conformidad:** Ayuda a cumplir con regulaciones de seguridad y conformidad al identificar y solucionar problemas de seguridad.

**Integración con otras herramientas:** Se integra con otras herramientas de desarrollo, como sistemas de control de versiones, gestión de proyectos y gestión de dependencias.

## Uso

Lo primero que vamos a hacer es crear un proyecto en SonarQube. Para ello, accedemos a la interfaz web de SonarQube y seleccionamos "Create Project" -> "Manually". Podemos optar por esta opción si ya tenemos un proyecto creado en SonarQube o si preferimos hacerlo manualmente. No necesitamos realizar una integración global, ya que no todos los proyectos que vamos a analizar pueden estar en el mismo proveedor de repositorios, como GitLab o GitHub.

![Crear proyecto](/img/sonarqube/create_project.png)

Esto nos llevará a una ventana donde se nos pedirá la siguiente información:

![Datos proyecto](/img/sonarqube/create_project_add_data.png)

- Nombre del proyecto.
- Clave única del proyecto (Key).
- Rama principal del proyecto.

Con estos datos, configuramos cómo queremos tratar el código nuevo. Tenemos tres opciones:

- "Versión previa": Considera como código nuevo cualquier cambio desde la versión anterior.
- "Número de días": Considera como código nuevo cualquier cambio realizado en los últimos x días.
- "Rama de referencia": Elige una rama como base para el código nuevo.

![Datos proyecto](/img/sonarqube/new_code.png)

Una vez creado el proyecto, procedemos a integrarlo con nuestro sistema de Integración Continua (CI). Para este caso, utilizaremos GitLab CI. En la configuración, generamos un token específico para nuestro proyecto en SonarQube. Este token nos guiará en los pasos necesarios para la integración.

![Configuración repositiro analizar](/img/sonarqube/analyze_repository.png)

El primer paso es generar un token, donde podemos ajustar su duración según nuestras necesidades. Es importante anotar este token, ya que una vez se muestra, no se mostrará de nuevo.

![Primer paso wizard](/img/sonarqube/wizard-1.png)

![Crear Token](/img/sonarqube/create_token.png)

![Token](/img/sonarqube/token.png)

Luego, seleccionamos el tipo de proyecto que estamos utilizando y se nos mostrarán algunas configuraciones adaptadas. En nuestro caso, veremos cómo enviar los resultados de las pruebas realizadas en el stage de testing para poder visualizar fácilmente los porcentajes de cobertura.

![Segundo paso wizard](/img/sonarqube/wizard-2.png)

Finalmente, llegamos al último paso y el sistema quedará a la espera de que se realice el primer commit de código para su análisis. Durante el análisis, podemos obtener información sobre errores, códigos problemáticos, vulnerabilidades, revisiones de seguridad, líneas duplicadas y cobertura de código.

![Tercer paso wizard](/img/sonarqube/wizard-3.png)

![Analisis](/img/sonarqube/analysis.png)