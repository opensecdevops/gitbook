# node

En el stage de preparación de node, es esencial gestionar las dependencias de manera eficiente antes de avanzar a las fases posteriores del ciclo de CI/CD. Aquí se realizarán las preparaciones clave:

**Instalación de todas las dependencias (desarrollo y producción)**: En esta etapa, se ejecutará una preparación que se encargará de instalar todas las dependencias necesarias para el proyecto. Esto incluirá tanto las dependencias de producción, que son esenciales para el funcionamiento de la aplicación en un entorno de producción, como las dependencias de desarrollo, que son útiles durante el proceso de desarrollo y pruebas. La instalación de todas las dependencias garantiza que el proyecto sea completamente funcional en este entorno de construcción.

## Integración con Gitlab

Como acabamos de mencionar, vamos a configurar un jobs.

### Todas las dependencias (node)

Para la preparación de las dependencias necesarias en la etapa de pruebas, crearemos un stage que compartiremos con el resto de los stages a través de la caché de GitLab Runner, lo que mejorará significativamente la eficiencia de nuestro pipeline. En este paso, no generaremos artefactos ya que no requeriremos su descarga posteriormente. La optimización de la etapa de preparación es esencial para garantizar que todas las pruebas posteriores se ejecuten de manera eficaz y sin problemas de dependencias faltantes. Al utilizar la caché de GitLab Runner, evitamos la necesidad de volver a descargar las mismas dependencias en cada etapa del pipeline, lo que acelera significativamente el proceso de construcción y pruebas de nuestra aplicación.

Este stage llamado `.dependencies_cache` se encarga de gestionar la caché de dependencias para el proyecto actual en GitLab CI/CD.

```yml title=".gitlab-ci.yml" showLineNumbers
.dependencies_cache:
  cache:
    key: $CI_PROJECT_NAME-osdo
    paths:
      - .npm/
      - node_modules/
      - public/build/
```

El punto delante del nombre de un stage en GitLab CI/CD, como por ejemplo `.dependencies_cache`, es una convención utilizada para indicar que se trata de un stage oculto o interno que no se ejecuta de forma independiente, sino que se utiliza como parte de otros stages o para realizar tareas específicas de configuración, preparación o limpieza en el pipeline, pero no se inicia por sí mismo.

En este caso, se está configurando la caché con la clave key definida como `$CI_PROJECT_NAME-osdo`. La clave es única para cada proyecto y se utiliza para identificar y recuperar la caché asociada a ese proyecto en particular.

Los elementos que se están almacenando en la caché se especifican en la lista bajo paths. En este ejemplo, se están incluyendo los siguientes directorios y sus contenidos:

- **.npm/**: Este directorio es utilizado por npm, el gestor de paquetes de Node.js, para almacenar las dependencias de JavaScript y Node.js utilizadas en el proyecto.

- **node_modules/**: Aquí se encuentran las dependencias y módulos de Node.js requeridos por la aplicación.

- **public/build/**: Contiene archivos generados en el proceso de construcción de la aplicación, como los recursos estáticos.

Una vez configurada la cache la podemos usar en nuestro stage de preparación

```yml title=".gitlab-ci.yml" showLineNumbers
npm:
  image: node@sha256:f4c96a28c0b2d8981664e03f461c2677152cd9a756012ffa8e2c6727427c2bda
  stage: preparation
  needs:
    - composer
  extends: .dependencies_cache
  before_script:
    - npm ci --cache .npm --prefer-offline
  script:
    - npm run build
```

Este stage llamado `npm` se encarga de instalar las dependencias de node utilizando npm durante la etapa de preparación en un pipeline de GitLab CI/CD.

- **image**: Especifica la imagen de Docker que se utilizará para ejecutar este stage. En este caso, se utiliza la imagen de node@sha256:f4c96a28c0b2d8981664e03f461c2677152cd9a756012ffa8e2c6727427c2bda con una etiqueta específica (sha256:f4c96a28c0b2d8981664e03f461c2677152cd9a756012ffa8e2c6727427c2bda) que proporciona un entorno configurado para Node y las herramientas necesarias.

- **stage**: Define la etapa a la que pertenece este stage. En este caso, está en la etapa de preparación, lo que significa que se ejecutará antes de otros stages en el pipeline.

- **needs**: Nos dice que depende del stage de composer esto es se puede dar por ejemplo en proyecto que son Laravel + InertiaJS + Vue como la APP de OSDO

- **before_script**: Aquí se especifican los comandos que se ejecutarán antes de los scripts del stage. En este caso, se utiliza el comando `npm ci --cache .npm --prefer-offline` para instalar las dependencias de node. Los diferentes parámetros que siguen a npm install tienen los siguientes significados:

  - **ci**: Evita modificar el archivo `package.json` ni los archivos de bloqueo de las dependencias `package-lock.json` o `yarn.lock`.
  - **--cache .npm**: Use la caché local para almacenar los paquetes descargados.
  - **--prefer-offlinen**: Intenta instalar los paquetes desde la caché local incluso si no están actualizados.

- **script**: Aquí se especifican los comandos que se ejecutarán en este stage. En este caso, se utiliza el comando `npm run build` para construir la aplicación, dicho comando esta definido en el `package.json`.

- **extends**: Utiliza la directiva extends para heredar la configuración de otro stage llamado .dependencies_cache. Esto es comúnmente utilizado para compartir configuraciones entre diferentes stages. En este caso, .dependencies_cache configura el uso de la caché de GitLab Runner para almacenar y recuperar dependencias previamente instaladas, lo que mejora significativamente el rendimiento del pipeline al evitar la descarga e instalación repetitiva de dependencias idénticas.