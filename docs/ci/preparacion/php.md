# PHP

En el stage de preparación de PHP, es esencial gestionar las dependencias de manera eficiente antes de avanzar a las fases posteriores del ciclo de CI/CD. Aquí se realizarán dos preparaciones clave:

**Instalación de todas las dependencias (desarrollo y producción)**: En esta etapa, se ejecutará una preparación que se encargará de instalar todas las dependencias necesarias para el proyecto. Esto incluirá tanto las dependencias de producción, que son esenciales para el funcionamiento de la aplicación en un entorno de producción, como las dependencias de desarrollo, que son útiles durante el proceso de desarrollo y pruebas. La instalación de todas las dependencias garantiza que el proyecto sea completamente funcional en este entorno de construcción.

**Instalación de dependencias de producción para la creación de imágenes optimizadas**: Para optimizar la construcción de imágenes de contenedor y reducir el tamaño de las mismas, se realizará una preparación adicional que se centrará exclusivamente en instalar las dependencias de producción. Estas dependencias de producción son las que realmente se ejecutarán en un entorno de producción, y separarlas en una preparación independiente permite crear imágenes de contenedor más ligeras y eficientes, eliminando la necesidad de incluir dependencias de desarrollo innecesarias en las imágenes de producción.

## Integración con Gitlab

Como acabamos de mencionar, vamos a configurar dos jobs. El primero estará en la etapa de preparación del pipeline y lo llamaremos "composer". El segundo estará en el stage de "build" y se llamará "composer_prod".

### Todas las dependencias (composer)

Para la preparación de las dependencias necesarias en la etapa de pruebas, crearemos un stage que compartiremos con el resto de los stages a través de la caché de GitLab Runner, lo que mejorará significativamente la eficiencia de nuestro pipeline. En este paso, no generaremos artefactos ya que no requeriremos su descarga posteriormente. La optimización de la etapa de preparación es esencial para garantizar que todas las pruebas posteriores se ejecuten de manera eficaz y sin problemas de dependencias faltantes. Al utilizar la caché de GitLab Runner, evitamos la necesidad de volver a descargar las mismas dependencias en cada etapa del pipeline, lo que acelera significativamente el proceso de construcción y pruebas de nuestra aplicación.

Este stage llamado `.dependencies_cache` se encarga de gestionar la caché de dependencias para el proyecto actual en GitLab CI/CD.

```yml title=".gitlab-ci.yml" showLineNumbers
.dependencies_cache:
  cache:
    key: $CI_PROJECT_NAME-osdo
    paths:
      - vendor/
```

El punto delante del nombre de un stage en GitLab CI/CD, como por ejemplo `.dependencies_cache`, es una convención utilizada para indicar que se trata de un stage oculto o interno que no se ejecuta de forma independiente, sino que se utiliza como parte de otros stages o para realizar tareas específicas de configuración, preparación o limpieza en el pipeline, pero no se inicia por sí mismo.

En este caso, se está configurando la caché con la clave key definida como `$CI_PROJECT_NAME-osdo`. La clave es única para cada proyecto y se utiliza para identificar y recuperar la caché asociada a ese proyecto en particular.

Los elementos que se están almacenando en la caché se especifican en la lista bajo paths. En este ejemplo, se están incluyendo los siguientes directorios y sus contenidos:

- **vendor/**: Este directorio generalmente se usa en proyectos PHP para almacenar las dependencias de terceros administradas por Composer.

Una vez configurada la cache la podemos usar en nuestro stage de preparación

```yml title=".gitlab-ci.yml" showLineNumbers
composer:
  image: harbor.opensecdevops.com/osdo/php-ci@sha256:c4d27b1286fb998148f7439533f83c2ca40f0358bbda6009a6a3f069e14086c0
  stage: preparation
  script:
      - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
  extends: .dependencies_cache
```

Este stage llamado `composer` se encarga de instalar las dependencias de PHP utilizando Composer durante la etapa de preparación en un pipeline de GitLab CI/CD.

- **image**: Especifica la imagen de Docker que se utilizará para ejecutar este stage. En este caso, se utiliza la imagen harbor.opensecdevops.com/osdo/php-ci con una etiqueta específica (sha256:c4d27b1286fb998148f7439533f83c2ca40f0358bbda6009a6a3f069e14086c0) que proporciona un entorno configurado para PHP y las herramientas necesarias.

- **stage**: Define la etapa a la que pertenece este stage. En este caso, está en la etapa de preparación, lo que significa que se ejecutará antes de otros stages en el pipeline.

- **script**: Aquí se especifican los comandos que se ejecutarán en este stage. En este caso, se utiliza el comando composer install para instalar las dependencias de PHP. Los diferentes parámetros que siguen a composer install tienen los siguientes significados:

  - **--prefer-dist**: Indica a Composer que prefiera descargar las dependencias desde un archivo comprimido si están disponibles, en lugar de clonar los repositorios de GitHub o de otros lugares.
  - **--no-ansi**: Evita la salida de caracteres de control ANSI en la salida, lo que hace que la salida sea más legible en un entorno de automatización como GitLab CI/CD.
  - **--no-interaction**: Ejecuta Composer en modo no interactivo, lo que significa que no esperará la entrada del usuario para confirmar acciones.
  - **--no-progress**: Deshabilita la visualización del progreso de la instalación de las dependencias.
  - **--no-scripts**: Evita que se ejecuten los scripts definidos en los paquetes de las dependencias. Esto es útil en entornos de CI/CD para acelerar la instalación.

- **extends**: Utiliza la directiva extends para heredar la configuración de otro stage llamado .dependencies_cache. Esto es comúnmente utilizado para compartir configuraciones entre diferentes stages. En este caso, .dependencies_cache configura el uso de la caché de GitLab Runner para almacenar y recuperar dependencias previamente instaladas, lo que mejora significativamente el rendimiento del pipeline al evitar la descarga e instalación repetitiva de dependencias idénticas.

### Dependencias de producción (composer_prod)

```yml title=".gitlab-ci.yml" showLineNumbers
composer_prod:
  image: harbor.opensecdevops.com/osdo/php-ci@sha256:c4d27b1286fb998148f7439533f83c2ca40f0358bbda6009a6a3f069e14086c0
  stage: build
  script:
      - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts --no-plugins --optimize-autoloader  --no-dev 
  artifacts:
    paths:
      - vendor/
```

Este stage llamado `composer_prod` se utiliza para instalar las dependencias de producción de PHP utilizando Composer durante la etapa de construcción en un pipeline de GitLab CI/CD. Vamos a ver la diferencias con el stage anterior:

- **stage**: Define la etapa a la que pertenece este stage. En este caso, está en la etapa de construcción, lo que significa que se ejecutará después de que se hayan preparado las dependencias en el stage anterior y los diferentes test.

- **script**: Aquí se especifican los comandos que se ejecutarán en este stage. La diferencia es que se han agregado 3 parametros más para optimizar las dependencias en producción.

  - **--no-scripts**: Evita que se ejecuten los scripts definidos en los paquetes de las dependencias.
  - **--no-plugins**: Deshabilita la ejecución de complementos durante la instalación.
  - **--optimize-autoloader**: Optimiza el cargador automático de Composer para mejorar el rendimiento de la aplicación en producción.

- **artifacts**: Define los artefactos que se deben conservar después de la ejecución de este stage. En este caso, se guarda la carpeta vendor/ que contiene las dependencias de producción de PHP y las pasaremos a la depedencia job que construye la imagen.
