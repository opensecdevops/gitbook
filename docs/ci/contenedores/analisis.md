# Analisis

El análisis de imágenes Docker en busca de problemas de seguridad es una práctica esencial en DevSecOps para identificar vulnerabilidades y riesgos en las imágenes de contenedores antes de desplegarlas en un entorno de producción. La herramienta Trivy se destaca como una solución valiosa para llevar a cabo este tipo de análisis. Trivy escanea las imágenes de Docker en busca de vulnerabilidades conocidas en bibliotecas y dependencias, lo que ayuda a los equipos de desarrollo y operaciones a tomar medidas proactivas para mitigar los riesgos de seguridad.

## Trivy

**Análisis de vulnerabilidades:** Trivy utiliza una amplia base de datos de vulnerabilidades conocidas en bibliotecas y paquetes de software para identificar problemas de seguridad en las imágenes Docker.

**Integración de CI/CD:** Trivy se puede incorporar fácilmente en pipelines de CI/CD para automatizar el escaneo de imágenes de contenedores en cada etapa del ciclo de desarrollo, desde la construcción hasta el despliegue.

**Escaneo rápido:** Trivy es conocido por su velocidad de escaneo rápida, lo que permite identificar rápidamente posibles problemas de seguridad sin ralentizar el proceso de desarrollo.

**Compatibilidad multiplataforma:** Trivy es compatible con diversas plataformas y registries de contenedores, lo que facilita su implementación en entornos heterogéneos.

**Integración con orquestadores de contenedores:** Trivy se puede utilizar con orquestadores populares de contenedores como Kubernetes para garantizar que las imágenes utilizadas en clústeres sean seguras.

**Informes detallados:** Proporciona informes detallados sobre las vulnerabilidades encontradas, incluyendo información sobre el impacto y las soluciones posibles.

**Actualizaciones regulares:** La base de datos de vulnerabilidades de Trivy se actualiza periódicamente para garantizar que las evaluaciones estén basadas en la información de seguridad más reciente.

**Personalización:** Trivy permite personalizar las políticas de escaneo y establecer umbrales de severidad para personalizar las respuestas a las vulnerabilidades detectadas.

### Integración con Gitlab

Para integrar trivy en el CI vamos a usar la imagen oficial en su versión latest en vez de usar el digest como hemos realizado durante todo el proyecto para poder tener disponibles la ultima información de las vulnerabilidades.

```yaml title=".gitlab-ci.yml" showLineNumbers
docker_scan:
  stage: build_test
  dependencies: ["docker_build"]
  variables:
    DD_SCAN_TYPE: "Trivy Scan"
    DD_PRODUCT_NAME: "Trivy"
    DD_SCAN_ACTIVE: "true"
    DD_SCAN_VERIFIED: "false"
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  script:
    - mkdir -p scan_result
    - cd tar_images
    - |
      for tar_image in *.tar; 
      do
        [ -e "$tar_image" ] || continue;
        file_name=${tar_image%.*};
        trivy image --timeout 15m --offline-scan --input $tar_image -f json -o ../scan_result/$file_name.json --severity CRITICAL; 
      done
    - | 
      cd ../scan_result
      ls;
      vulns=false;
      for result in *.json;
      do
        [ -e "$result" ] || continue;
        file_name=${result%.*};
        vulnerabilities=$(awk -F '[:,]' '/"Vulnerabilities"/ {gsub("[[:blank:]]+", "", $2); print $2}' "$file_name.json");
        if ! [ -z "$vulnerabilities" ]; then 
          vulns=true;
          DD_SCAN_FILE=$file_name.json;
          bash .gitlab-ci/defectdojo-finding.sh
        fi
      done
      if [ "$vulns" = true ]; then
        exit 1;
      fi
  artifacts:
    paths:
      - scan_result
    expire_in: 1 month
```

El job denominado `docker_scan` se encuentra en el escenario (stage) de "build_test" y depende del trabajo anterior llamado "docker_build". Su función principal es realizar un análisis de seguridad en las imágenes Docker que fueron construidas en el trabajo anterior utilizando una herramienta llamada "Trivy Scan" y luego enviar los resultados a DefectDojo.

- **dependencies**: ["docker_build"]: Este stage depende del resultado exitoso del stage docker_build, lo que nos permite realizar el analisis

- **variables**: Define varias variables de entorno que se utilizarán en el proceso de análisis y envío de resultados a DefectDojo. Estas variables incluyen el tipo de análisis, el nombre del producto, la activación del análisis y su estado de verificación.

- **image**: Este trabajo utiliza la imagen Docker "docker.io/aquasec/trivy:latest" para ejecutar el análisis de seguridad.

- **script**:  En esta sección, se ejecuta un script que realiza las siguientes acciones:

  - El trabajo comienza creando un directorio llamado "scan_result" para almacenar los resultados del análisis.
  - Luego, accede al directorio que contiene las imágenes Docker construidas anteriormente ("tar_images"). Utiliza un bucle para iterar sobre todas las imágenes en formato "tar" y ejecuta el análisis de seguridad con "Trivy". Los resultados del análisis se almacenan en archivos JSON en el directorio "scan_result".
  - Después de completar el análisis de seguridad de cada imagen, el trabajo verifica si se encontraron vulnerabilidades críticas en el análisis. 
  - Si se detectan vulnerabilidades, el trabajo configura la variable "DD_SCAN_FILE" con el nombre del archivo JSON que contiene los detalles de las vulnerabilidades. Luego, llama al script "defectdojo-finding.sh" para enviar estos resultados a DefectDojo.
  - Si existen vulnerabilidades el job acaba con exit 1 dando error.

- **artifacts**: Los resultados del análisis se almacenan como artefactos y se conservan durante un mes. Esto significa que los resultados del análisis están disponibles para su revisión y auditoría durante ese período.