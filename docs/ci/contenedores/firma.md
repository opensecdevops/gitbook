# Firma

Firmar imágenes Docker utilizando herramientas como "cosign" es una práctica esencial para garantizar la integridad y autenticidad de las imágenes que se utilizan en entornos de desarrollo, pruebas y producción. Al firmar una imagen Docker, se agrega una capa adicional de seguridad que permite a los equipos de SecDevOps verificar que la imagen no ha sido alterada desde su creación y que proviene de una fuente confiable.

## Caracteristicas

1. **Integridad y autenticidad**: La firma de imágenes Docker permite verificar que una imagen no ha sido modificada y que proviene de la fuente esperada. Esto ayuda a prevenir la ejecución de imágenes comprometidas o modificadas de manera maliciosa.

2. **Verificación de fuente confiable**: Con "cosign", se pueden utilizar claves de firma criptográfica para verificar que la imagen proviene de una fuente confiable y que no ha sido manipulada durante la distribución.

3. **Protección contra ataques y vulnerabilidades**: Al verificar la firma de una imagen antes de su implementación, se reduce el riesgo de implementar imágenes con vulnerabilidades conocidas o malware.

4. **Cumplimiento y auditoría**: La firma de imágenes Docker es una práctica recomendada para cumplir con regulaciones y estándares de seguridad, ya que proporciona un registro de control y auditoría de las imágenes utilizadas en el ciclo de desarrollo.

5. **Integración en pipelines de CI/CD**: "cosign" y otras herramientas de firma de imágenes se pueden integrar fácilmente en pipelines de CI/CD para automatizar la firma y verificación de imágenes durante el proceso de construcción y despliegue.

6. **Compatibilidad con registros de contenedores**: Estas herramientas son compatibles con varios registros de contenedores, lo que permite firmar y verificar imágenes en entornos como Docker Hub, Google Container Registry, y otros.

## Integración con Gitlab

Para poder firmar los contenedores en el CI/CD lo primero que hacer es crear un par de claves rsa para su firma, hay que dejar las claves en blanco, dado que no hay manera de intro

```bash
$ cosign generate-key-pair
Enter password for private key:
Enter again:
Private key written to cosign.key
Public key written to cosign.pub
```

```yml title=".gitlab-ci.yml" showLineNumbers
docker_sign:
  stage: sign_image
  dependencies: ["docker_push"]
  variables:
    GIT_STRATEGY: none
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  image: docker:24.0.7
  services:
    - docker:24.0.7-dind
  before_script:
    - apk add --update cosign curl bash
    - curl -s https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer | bash
  script:
    - echo "$DOCKER_REGISTRY_PASS" | docker login -u "$DOCKER_REGISTRY_USER" --password-stdin $DOCKER_REGISTRY_URL
    - cosign sign --key .secure_files/cosign.key -y $DOCKER_REGISTRY_URL/osdo/osdo-app@${DIGEST}
```
