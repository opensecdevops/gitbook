# Construcción

La construcción de imágenes en Docker es el proceso de crear una imagen de contenedor que contiene todo lo necesario para ejecutar una aplicación: desde el sistema operativo base hasta las dependencias y configuraciones específicas de la aplicación.

Para ello en un Kubernetes hacemos uso de la siguente herramienta:

## Kaniko

Kaniko es una herramienta fantástica para construir imágenes de contenedores dentro de un entorno de Kubernetes sin necesidad de privilegios de root! Aquí tienes algunos detalles clave sobre Kaniko:

**Características Principales:**
Construcción de Imágenes en Kubernetes: Kaniko es una herramienta de construcción de imágenes de contenedores que funciona en entornos no privilegiados, como clústeres de Kubernetes. Esto significa que puede ejecutarse sin necesidad de acceso de superusuario, lo que lo hace ideal para entornos controlados y más seguros.

**Compatibilidad con Dockerfile:** Utiliza el archivo Dockerfile como base para la construcción de la imagen. Acepta la misma sintaxis y comandos que Docker para definir los pasos necesarios para crear la imagen.

**Cacheado de Capas:** Kaniko emplea un sistema de cacheado de capas inteligente que ayuda a acelerar los tiempos de construcción al aprovechar las capas de caché existentes para las instrucciones del Dockerfile que no han cambiado.

**Ejecución como Contenedor:** Se ejecuta como un contenedor dentro del clúster de Kubernetes. Esto significa que la construcción de la imagen se lleva a cabo de manera aislada del host donde se está ejecutando, lo que añade una capa adicional de seguridad y portabilidad.

**Beneficios:**
Seguridad Mejorada: Al no requerir privilegios de root, Kaniko mejora la seguridad al construir imágenes dentro de entornos restringidos como Kubernetes.

**Portabilidad:** Permite la construcción de imágenes sin depender de un entorno local, lo que facilita la portabilidad y la integración con flujos de trabajo de CI/CD (Continuous Integration/Continuous Deployment).

**Optimización de Recursos:** Al utilizar Kubernetes, Kaniko aprovecha eficientemente los recursos del clúster para realizar las tareas de construcción de imágenes, distribuyendo la carga de trabajo de manera eficaz.

**Compatibilidad con Repositorios Privados:** Puede autenticarse y acceder a repositorios privados para obtener capas de imágenes base durante el proceso de construcción.

**Uso:**
Para utilizar Kaniko, normalmente se crea un manifiesto de Kubernetes (por ejemplo, un archivo YAML) que describe el trabajo de construcción de la imagen utilizando Kaniko como un pod dentro del clúster. Este manifiesto especifica la ubicación del Dockerfile, el contexto de construcción y otros detalles necesarios.

**Comunidad y Mantenimiento:**
Kaniko es un proyecto de código abierto mantenido por la comunidad y tiene una base de usuarios activa. Es parte del ecosistema de herramientas de Kubernetes y se integra bien con flujos de trabajo modernos de CI/CD.

## Integración con Gitlab

```yml title=".gitlab-ci.yml" showLineNumbers
.docker_build:
  stage: build
  dependencies: ['pre_build_app']
  image:
    name: gcr.io/kaniko-project/executor@sha256:e935bfe222bc3ba84797e90a95fcfb1a63e61bcf81275c2cac5edfb0343b2e85
    entrypoint: [""]
  script:
    - mkdir tar_images
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/deployment/php/Dockerfile"
      --destination "${DOCKER_REGISTRY_URL}/osdo/osdo-app:${CI_COMMIT_TAG}" --no-push --tar-path tar_images/osdo_app.tar
  artifacts:
    paths:
      - tar_images
    when: on_success
```

Vamos a explicar las cosas mas importantes de este Job:

- **dependencies: ['pre_build_app']**: El job .docker_build puede tener otras dependencias asociadas si la aplicación que vamos a agregar al contenedor depende de jobs previos donde se hizo la construcción de la aplicación para realizar los test o su compilación.

- **image**: Este job utiliza una imagen base para ejecutar Kaniko. La imagen utilizada es gcr.io/kaniko-project/executor:latest.

- **script**: En esta sección, se realiza la construcción de la imagen de Docker utilizando Kaniko. Los pasos incluyen la creación de un directorio llamado tar_images, que se utiliza para almacenar el archivo tar de la imagen de Docker. Luego, se ejecuta Kaniko con varios argumentos:

  - `--context "${CI_PROJECT_DIR}"`: Esto especifica el contexto de construcción como el directorio raíz del proyecto de GitLab CI.
  - `--dockerfile "${CI_PROJECT_DIR}/deployment/php/Dockerfile"`: Aquí se proporciona la ubicación del Dockerfile que se utilizará para construir la imagen. El Dockerfile se encuentra en el directorio deployment/php del proyecto.
  - `--destination "${DOCKER_REGISTRY_URL}/osdo/osdo-app:${CI_COMMIT_TAG}"`: Esto define la ubicación de destino donde se guardará la imagen de Docker construida. Utiliza la URL del registro de Docker y agrega una etiqueta basada en la etiqueta del commit actual.
  - `--no-push`: Esta opción evita que Kaniko cargue la imagen directamente en un registro de Docker. En su lugar, la imagen se guarda en un archivo tar local.
  - `--tar-path tar_images/osdo_app.tar`: Especifica la ubicación donde se guardará el archivo tar de la imagen de Docker, que es el directorio tar_images.

- **artifacts**: Esta sección define los artefactos que se guardarán después de que se complete este job. En este caso, se está archivando el directorio tar_images que contiene el archivo tar de la imagen de Docker.

- **when**: on_success: Indica que los artefactos se guardarán solo si este job se ejecuta con éxito, lo que significa que la imagen de Docker se construyó correctamente.
