# Linting

El linting en Docker, utilizando herramientas como [Hadolint](https://github.com/hadolint/hadolint), es esencial para garantizar la seguridad y la calidad de las imágenes de contenedor. Esta práctica identifica vulnerabilidades, configuraciones inseguras y errores en los archivos Dockerfile, lo que es crucial para prevenir exposiciones de vulnerabilidades conocidas y garantizar la construcción segura de imágenes. Además, el linting promueve la consistencia y el cumplimiento de las mejores prácticas, lo que facilita la replicación y la colaboración en equipos de desarrollo y operaciones. En última instancia, el linting de Docker mejora la eficiencia del proceso de desarrollo y despliegue, ahorra tiempo y recursos, y contribuye a la confiabilidad y la calidad del software en entornos de contenedor.

Si deseas probar Hadolint y comenzar a aplicar linting de Docker de manera inmediata, puedes acceder a la herramienta en línea en el siguiente enlace: Hadolint Online. Esta plataforma te permitirá analizar tus archivos Dockerfile de forma interactiva y recibir recomendaciones y correcciones en tiempo real, lo que facilita la adopción de buenas prácticas de seguridad y calidad en tus imágenes de contenedor.

## Integración con Gitlab

Para integrar Hadolint con CI/CD de Gitlab, tenemos dos opciones: utilizar la imagen disponible en el [DockerHub](https://hub.docker.com/r/hadolint/hadolint) o la que tenemos en el proyecto [OSDO](https://harbor.opensecdevops.com/harbor/projects/2/repositories/hadolint/artifacts-tab?publicAndNotLogged=yes), que ya tiene instalado Curl para permitir la integración con DefectDojo.

Una vez que hayamos decidido cuál opción necesitamos, procedemos a añadir la siguiente sección en nuestro archivo `gitlab-ci.yml`:

```yml title=".gitlab-ci.yml" showLineNumbers
docker_hadolit:
  stage: test
  image:  image_need
  script:
    - hadolint "${CI_PROJECT_DIR}/deployment/php/Dockerfile" -f json > hadolint.json
  artifacts:
    paths:
      - hadolint.json
    when: on_failure
    expire_in: 1 hour
```

## DefectDojo

Para realizar la integración con DefectDojo, debemos agregar la opción `after_script` en nuestro archivo de configuración de GitLab CI/CD. En este paso, llamaremos al script `defectdojo-finding.sh`", que se encuentra en la sección de [Integraciones de Monitorización](../integraciones-monitorizacion.md#findings). Para asegurarnos de que la llamada al script solo se realice si se han encontrado "fallos", lo envolvemos en una estructura condicional `if` que verifica el estado del trabajo con la variable `$CI_JOB_STATUS`. También debemos agregar las siguientes variables de configuración para personalizar el "finding": `$DD_SCAN_TYPE,` `$DD_PRODUCT_NAME`, `$DD_SCAN_FILE`, `$DD_SCAN_ACTIVE`, `$DD_SCAN_VERIFIED`.

```yml title=".gitlab-ci.yml" showLineNumbers
docker_hadolit:
  stage: test
  dependencies: ["defectdojo_create_engagement"]
  variables:
    DD_SCAN_TYPE: "Hadolint Dockerfile check"
    DD_PRODUCT_NAME: "Hadolint"
    DD_SCAN_FILE: "hadolint.json"
    DD_SCAN_ACTIVE: "true"
    DD_SCAN_VERIFIED: "false"
  image: image_need
  script:
    - hadolint "${CI_PROJECT_DIR}/deployment/php/Dockerfile" -f json > hadolint.json
  after_script:
    - |
      if [ $CI_JOB_STATUS == 'failed' ]; then
        bash .gitlab-ci/defectdojo-finding.sh
      fi
  artifacts:
    paths:
      - hadolint.json
    when: on_failure
    expire_in: 1 hour
```
