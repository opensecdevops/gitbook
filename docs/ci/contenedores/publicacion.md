# Publicación

Publicar imágenes Docker en un registro (registry) es un paso esencial para compartir y distribuir las imágenes de contenedor que has construido. 

Para ello hacemos uso en este proyecto de la siguiente herramienta:

## Crane

Crane es una herramienta que facilita la manipulación y la gestión de imágenes Docker en registros remotos. Está especialmente diseñada para trabajar con registros alternativos al registro predeterminado de Docker, como Amazon ECR, Google Container Registry, Azure Container Registry, entre otros.

**Funcionalidades Clave de Crane:**
Interfaz Simplificada: Ofrece una interfaz simple de línea de comandos para gestionar imágenes en registros diferentes al registro predeterminado de Docker.

**Soporte para Registros Alternativos:** Crane permite trabajar con una variedad de registros, lo que lo hace útil para proyectos que no se basan en Docker Hub como su principal repositorio de imágenes.

**Autenticación y Manejo de Credenciales:** Facilita la autenticación y el manejo de credenciales para interactuar con registros remotos, permitiendo a los usuarios autenticarse fácilmente para acceder y manipular imágenes en estos registros.

**Push y Pull de Imágenes:** Permite enviar (push) y recuperar (pull) imágenes desde y hacia registros remotos, de manera similar a cómo se trabaja con el registro predeterminado de Docker.

**Gestión de Etiquetas y Versiones:** Crane ofrece opciones para gestionar etiquetas y versiones de imágenes, lo que facilita la identificación y la manipulación de versiones específicas de las imágenes.

**Ventajas de Utilizar Crane:**

1. **Facilita la Migración:** Para proyectos que migran entre diferentes registros de imágenes, Crane simplifica el proceso al proporcionar una herramienta dedicada para la manipulación de imágenes.

2. **Flexibilidad en la Elección del Registro:** Permite a los equipos utilizar registros alternativos al Docker Hub según las necesidades de su infraestructura y políticas de seguridad.

3. **Automatización en Flujos de Trabajo:** Al ser compatible con una variedad de registros, puede integrarse fácilmente en flujos de trabajo automatizados y en scripts de CI/CD.

**Uso de Crane:**
Instalación: Crane se puede instalar como una herramienta independiente y luego se utiliza a través de la línea de comandos.

1. **Comandos Básicos:** Ofrece comandos como crane pull, crane push, crane auth, entre otros, que son análogos a los comandos de Docker pero adaptados para trabajar con registros remotos.

2. **Configuración de Credenciales:** Antes de interactuar con registros remotos, se requiere configurar las credenciales correspondientes para la autenticación.

**Limitaciones:**
Si bien Crane es útil para trabajar con registros alternativos, puede tener algunas limitaciones en comparación con las funcionalidades más completas y específicas que ofrecen las herramientas nativas de los propios registros remotos.

### Uso

#### Integración en Gitlab

Lo primero que tenemos que hacer es registrar las credenciales de nuestro registrador de contenedores en las variables del CI/CD de gitlab, para ellos vamos a Harbor y creamos una cuenta de robot, podemos hacerla global o por proyecto.

Como nuestra idea siempre es pensar en la seguridad, tenemos que configurarlo con los minimos privilegios los cuales vemos en la imagen.

![Permisos](/img/harbor/permisions.png)

Creamos nuestro token y lo agregamos a las variables.

![Token](/img/harbor/token.png)

![variables CI](/img/harbor/variables.png)

Ahora que tenemos preparado el usuarios agregamos podemos publicar la imagen generada previamente en el job `docker_build`

```yml title=".gitlab-ci.yml" showLineNumbers
docker_push:
  stage: push_image
  dependencies: ["docker_build"]
  variables:
    GIT_STRATEGY: none
  image:
    name: gcr.io/go-containerregistry/crane:v0.16.1
    entrypoint: [""]
  script:
    - cd tar_images
    - |
      crane auth login -u $DOCKER_REGISTRY_USER -p $DOCKER_REGISTRY_PASS $DOCKER_REGISTRY_URL;
      for tar_image in *.tar; 
      do 
        crane push $tar_image $DOCKER_REGISTRY_URL/osdo/osdo-app:$CI_COMMIT_SHORT_SHA -v;
        DIGEST=$(crane digest $DOCKER_REGISTRY_URL/osdo/osdo-app:$CI_COMMIT_SHORT_SHA);
        echo "DIGEST=${DIGEST}" >> digest.env
      done
  artifacts:
    reports:
      dotenv: tar_images/digest.env
    expire_in: 10 min
```

El stage llamado `docker_push` se encuentra en la etapa `push_image` del ciclo de CI/CD y se encarga de realizar el empuje de las imágenes Docker que se han construido previamente en el stage `docker_build`.

- **dependencies**: ["docker_build"]: Este stage depende del resultado exitoso del stage docker_build, lo que garantiza que las imágenes a empujar estén construidas correctamente antes de proceder con la publicación.

- **variables**: GIT_STRATEGY: none: Se establece la variable GIT_STRATEGY en none, lo que indica que no se requiere ninguna estrategia específica de Git para este stage, ya que se trata principalmente de operaciones de contenedor.

- **image**: Se utiliza la imagen Docker gcr.io/go-containerregistry/crane:v0.16.1 para ejecutar las operaciones relacionadas con el empuje de imágenes. Esta imagen incluye la herramienta crane, que se utiliza para interactuar con registros de contenedores.

- **script**: En esta sección, se ejecuta un script que realiza las siguientes acciones:

  - Autentica con el registro de contenedores utilizando las credenciales proporcionadas en las variables de entorno $DOCKER_REGISTRY_USER y $DOCKER_REGISTRY_PASS, junto con la URL del registro $DOCKER_REGISTRY_URL.
  - Cambia al directorio tar_images, que es donde se encuentran las imágenes Docker en formato .tar que se han construido anteriormente.
  - Itera a través de cada archivo .tar en el directorio.
  - Luego, utiliza crane para empujar la imagen al registro especificando la etiqueta de la imagen como $CI_COMMIT_SHORT_SHA (que generalmente es una versión única generada por GitLab).
  - Captura el valor del "digest" de la imagen recién empujada y lo almacena en un archivo llamado digest.env.

- **artifacts**: Se configura esta sección para recopilar y almacenar el archivo digest.env como un artefacto. Esto es útil para tener un registro del digest de la imagen subida, que usaremos en siguientes etapas para poder firmarla.
