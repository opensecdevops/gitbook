# PHP

PHPUnit es un popular framework de pruebas unitarias para PHP que se utiliza ampliamente en el desarrollo de aplicaciones PHP. Su principal función es facilitar la escritura y ejecución de pruebas unitarias automatizadas para verificar el comportamiento y la calidad del código PHP.

## Características

**Framework de pruebas robusto:** PHPUnit proporciona una infraestructura sólida para la creación y ejecución de pruebas unitarias. Permite a los desarrolladores escribir pruebas de manera efectiva para verificar el funcionamiento de funciones, métodos y clases en su código.

**Soporte para diversas funcionalidades:** Además de las pruebas unitarias básicas, PHPUnit admite una variedad de tipos de pruebas, como pruebas de integración y pruebas funcionales. Esto permite evaluar diferentes aspectos del software, desde la lógica interna de las funciones hasta la interacción de componentes completos.

**Generación de informes de pruebas:** PHPUnit genera informes detallados de las pruebas, lo que facilita la identificación de fallas y errores en el código. Estos informes ayudan a los desarrolladores a depurar y corregir problemas de manera más eficiente.

**Herramientas de cobertura de código:** PHPUnit ofrece herramientas de medición de cobertura de código que permiten evaluar qué porcentaje del código fuente se ha ejecutado durante las pruebas. Esto ayuda a identificar áreas no probadas y a mejorar la calidad del código.

**Compatibilidad con CI/CD:** PHPUnit se puede integrar con una variedad de marcos de trabajo y herramientas de CI/CD, lo que facilita la incorporación de pruebas automatizadas en flujos de trabajo de desarrollo y garantiza que las pruebas se ejecuten de manera consistente.

**Extensibilidad:** PHPUnit es altamente extensible y permite a los desarrolladores crear sus propias extensiones y personalizaciones para satisfacer sus necesidades específicas de prueba.

## Integración con Gitlab

```yml title=".gitlab-ci.yml"
phpunit:
  stage: test
  image: harbor.opensecdevops.com/osdo/php-ci@sha256:c4d27b1286fb998148f7439533f83c2ca40f0358bbda6009a6a3f069e14086c0
  dependencies: ['composer']
  variables:
    XDEBUG_MODE: coverage
  script:
    - ./vendor/bin/phpunit --coverage-text --colors=never --log-junit phpunit-report.xml --coverage-clover coverage-report.xml
  artifacts:
    paths:
      - phpunit-report.xml
      - coverage-report.xml
    reports:
      junit: phpunit-report.xml
    expire_in: 30 min
```

- **image**: Se utiliza una imagen de contenedor específica que contiene el entorno necesario para ejecutar las pruebas PHPUnit. La imagen se recupera de un registro de contenedores (en este caso, Harbor) y se identifica por su hash sha256. Esto asegura que se utilice una versión específica de la imagen para garantizar la consistencia en las pruebas.

- **dependencies**: Se establecen dependencias en otros jobs del pipeline, como "composer". Esto asegura que estos jobs se ejecuten antes de que este job pueda comenzar, lo que podría ser necesario para instalar las dependencias requeridas para las pruebas.

- **variables**: Se establece la variable de entorno "XDEBUG_MODE" en "coverage", lo que indica que se activará la cobertura de código de Xdebug durante las pruebas. Esto es útil para medir la cobertura de código durante las pruebas unitarias.

- **script:** En esta sección, se ejecutan los comandos necesarios para ejecutar las pruebas PHPUnit. Esto incluye la ejecución del binario de PHPUnit ubicado en el directorio "vendor/bin" y la generación de informes de cobertura de código y de informes JUnit. Los resultados se almacenan en archivos específicos.

  - **--coverage-text**: Este parámetro indica a PHPUnit que genere un informe de cobertura de código en formato de texto después de ejecutar las pruebas. Proporciona un resumen simple de la cobertura de código en la consola.

  - **--colors=never**: Este parámetro desactiva la coloración de la salida en la consola. Es útil cuando se ejecutan las pruebas en un entorno que no admite colores, como un entorno de automatización o integración continua.

  - **--log-junit archivo-xml**: Este parámetro permite especificar un archivo XML en el cual se almacenarán los resultados de las pruebas en formato JUnit. El archivo XML es útil cuando se desea integrar las pruebas con herramientas de CI/CD u otras herramientas de informes.

  - **--coverage-clover archivo-xml**: Con este parámetro, se especifica un archivo XML donde se guardará un informe de cobertura de código en formato Clover. Este informe es útil para evaluar la cobertura de código en detalle.

- **artifacts**: Se definen los artefactos generados por este job que se deben conservar después de que finalice el pipeline. En este caso, se conservan dos archivos: "phpunit-report.xml" y "coverage-report.xml". Además, se define un informe JUnit que utiliza el archivo "phpunit-report.xml".