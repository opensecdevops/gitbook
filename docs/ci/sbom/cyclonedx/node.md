# Node

Para instalar [CycloneDX para Node con npm](https://github.com/CycloneDX/cyclonedx-node-npm), primero debes asegurarte de que tengas [node y npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) instalados en tu sistema. Luego, sigue estos pasos:

* Abre una terminal o línea de comandos.
* Instala el paquete `@cyclonedx/cyclonedx-npm` de manera global

```bash
npm install -g @cyclonedx/cyclonedx-npm
```

* Ya  podemos ejecutar el análisis de dependencias mediante CycloneXD con el siguiente comando

```bash
cyclonedx-npm --output-file sbom.json 
```

Podemos agregar `--omit dev` si no queremos que se analice las dependencias de desarrollo

## Integración con Gitlab

Ahora que sabemos como se integra en node vamos a agregar nuestro stage en el CI para que se ejecute en cada commit y se envie a la plataforma de gestión de dependencias Dependecy-track.

```yml title=".gitlab-ci.yml" showLineNumbers
sbom_scan_node:
  stage: sbom_scan
  image: harbor.opensecdevops.com/osdo/cyclonedx-npm@sha256:692c7d4a163548c90cf6b0d041ae99e113ef33c841aac3cd58c8ac6fc3c04a9f
  dependencies: ['npm']
  script:
    - cyclonedx-npm --output-file sbom.json --omit dev --short-PURLs
    - sh .gitlab-ci/dependency-track.sh 
  allow_failure: true
  artifacts:
    expire_in: 1 days
    paths:
      - sbom.json
    reports:
      cyclonedx: sbom.json
```

Una vez cargado el fichero sbom en la plataforma de Dependecy-track veremos las dependencias y su estado de actualización y de vulnerabilidades conocidas.

![Lista dependencias](/img/dependency-track/sbom_load.png)
