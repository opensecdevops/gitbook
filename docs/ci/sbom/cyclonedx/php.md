# PHP

Para instalar [CycloneDX en PHP con Composer](https://github.com/CycloneDX/cyclonedx-php-composer), primero debes asegurarte de que tengas PHP y [Composer](https://getcomposer.org/) instalados en tu sistema. Luego, sigue estos pasos:

* Abre una terminal o línea de comandos.
* Navega hasta la raíz de tu proyecto PHP, donde se encuentra tu archivo `composer.json`.
* Ejecuta el siguiente comando para agregar CycloneDX como una dependencia de desarrollo:

```bash
composer require --dev cyclonedx/cyclonedx-php-composer
```

* Permitimos el plugin de CycloneDX, para eso tenemos que agregar al `composer.json`  lo siguiente

```json
    "config": {
        "allow-plugins": {
            "cyclonedx/cyclonedx-php-composer": true
        }
    }
```

* Ya  podemos ejecutar el análisis de dependencias mediante CycloneXD con el siguiente comando

```bash
composer CycloneDX:make-sbom --output-file=sbom.json --output-format=json
```

## Integración con Gitlab

Ahora que sabemos como se integra en php vamos a agregar nuestro stage en el CI para que se ejecute en cada commit y se envie a la plataforma de gestión de dependencias Dependecy-track.

```yml title=".gitlab-ci.yml"
sbom_scan:
  stage: sbom_scan
  image: harbor.opensecdevops.com/osdo/php-ci@sha256:d44a5d14ce250fd5881fbdbceec02a3a43118ceb4d3abcb66861bec160becbbe
  dependencies: ['composer']
  variables:
    COMPOSER_ALLOW_SUPERUSER: 1
  cache:
    key: ${CI_COMMIT_REF_SLUG}-composer
    paths:
      - vendor/
  script:
    - composer CycloneDX:make-sbom --output-file=sbom.json --output-format=json
    - sh ../.gitlab-ci/dependency-track.sh 
  allow_failure: true
  artifacts:
    expire_in: 1 days
    paths:
      - sbom.json

```

Una vez cargado el fichero sbom en la plataforma de Dependecy-track veremos las dependencias y su estado de actualización y de vulnerabilidades conocidas.

![Lista dependencias](/img/dependency-track/sbom_load.png)
