# OWASP ZAP

OWASP ZAP (Zed Attack Proxy) es una herramienta de código abierto ampliamente utilizada para realizar pruebas de seguridad automatizadas y manuales en aplicaciones web. Desarrollada por la comunidad de seguridad de OWASP, ZAP se ha convertido en una opción esencial para los profesionales de la seguridad y los desarrolladores de aplicaciones web que desean identificar y mitigar vulnerabilidades. Su interfaz fácil de usar y sus poderosas capacidades lo convierten en una herramienta valiosa en la lucha contra las amenazas de seguridad en aplicaciones en línea.

## Características Principales

**Escaneo automatizado:** ZAP puede realizar escaneos automatizados para detectar una amplia gama de vulnerabilidades comunes, como inyecciones SQL, XSS, CSRF y muchas más.

**Exploración de aplicaciones:** Puede mapear la estructura de una aplicación web, identificando todas sus páginas y funcionalidades, lo que facilita la detección de posibles vulnerabilidades.

**Ataques de fuzzing:** Permite realizar ataques de fuzzing para probar la resistencia de la aplicación a entradas no válidas o maliciosas.

**Spidering avanzado:** ZAP realiza spidering avanzado para recopilar datos sobre la aplicación, identificar vulnerabilidades ocultas y generar mapas de sitio web.

**Interceptación de Proxy:** Actúa como proxy entre el navegador y la aplicación, lo que permite a los usuarios interceptar y modificar solicitudes y respuestas HTTP para pruebas manuales.

**Soporte de scripts:** ZAP admite scripts personalizados en varios lenguajes, lo que permite a los usuarios realizar pruebas específicas y automatizar tareas repetitivas.

**Generación de informes:** Genera informes detallados que incluyen descripciones de vulnerabilidades, ubicaciones y recomendaciones para su corrección.

**Integración CI/CD:** Puede integrarse en pipelines de CI/CD para realizar pruebas automáticas en cada etapa del ciclo de desarrollo.

**Autenticación y sesiones:** Permite configurar autenticación y gestionar sesiones para probar aplicaciones que requieren inicio de sesión.

**Soporte activo:** ZAP es respaldado por una comunidad activa de usuarios y desarrolladores que brindan soporte y mantienen actualizadas sus capacidades de detección.

**Personalización:** Los usuarios pueden personalizar fácilmente el alcance y la configuración de las pruebas para adaptarse a sus necesidades específicas.

## Uso

Para poder usarlo tenemos que usar la parte de sistema automatico de OWASP ZAP si quieres ampliar la información es muy recomendable el siguiente [vídeo](https://www.youtube.com/watch?v=TTiW5NPJlwY).

### Configuración contexto

Para poder utilizar OWASP ZAP en nuestro CI, lo primero que debemos hacer es configurarlo desde la interfaz para establecer la instancia que se usará en el pipeline.

Abre OWASP ZAP y haz clic en `Default Context` en la barra lateral izquierda. En la ventana que se abre, selecciona `Authentication`, donde podrás configurar cómo se realiza el inicio de sesión en nuestra aplicación.

![Authentication Context](/img/owasp/auth.png)

Disponemos de los siguientes tipos de autenticación:

- **Autenticación manual**: Permite a los usuarios autenticarse manualmente, como en un navegador usando proxy, seleccionando después la sesión HTTP. No soporta reautenticación, pero ZAP mantiene estadísticas con estrategias de verificación.

- **Autenticación basada en formularios**: Utiliza envío de formulario o solicitud GET para autenticarse con credenciales usuario/contraseña. Soporta reautenticación y configuración a través de la pantalla Autenticación de contextos de sesión o menús contextuales.

- **Autenticación basada en JSON**: Similar a la autenticación basada en formularios, pero utiliza un objeto JSON enviado a una URL específica. Permite reautenticación y configuración de credenciales usuario/contraseña.

- **Autenticación HTTP/NTLM**: Emplea mecanismos HTTP o NTLM con encabezados en mensajes HTTP. Soporta esquemas de autenticación Básico, Digest, NTLM y permite reautenticación con envío de encabezados en cada solicitud.

- **Autenticación basada en scripts**: Ideal para procesos de autenticación complejos, requiere definir un script personalizado que maneje la autenticación. Permite reautenticación y configuración mediante parámetros definidos en el script

En nuestro caso, vamos a utilizar la autenticación basada en formularios. Para ello, necesitamos los siguientes datos:

- La URL a la que se envían los datos del formulario.
- La URL donde se encuentra el formulario de inicio de sesión.
- Los datos que se envían en el método POST.
- Los parámetros que se van a reemplazar con el usuario y la contraseña.
- La estrategia de verificación para determinar si estamos o no autenticados.
- Una expresión regular (regex) para identificar si estamos autenticados o desautenticados.

