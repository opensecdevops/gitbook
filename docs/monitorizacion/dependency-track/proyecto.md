# Proyecto

En Dependency-Track, un "proyecto" es una entidad que representa una aplicación, servicio o proyecto específico que está siendo desarrollado y monitoreado. Sirve para organizar y aislar los componentes, bibliotecas y otras dependencias que son específicas para el desarrollo y mantenimiento de esa aplicación en particular.

## Creación

Para crear un producto, debemos dirigirnos a la barra lateral, hacer clic en el icono de sitemap, lo que nos llevará a todos los proyectos. En la página web que aparece, hacemos clic en el botón "Create Project", apareciendo así la siguiente ventana.

![Crear proyecto](/img/dependency-track/create_project.png)

En este formulario tendremos los siguientes campos a rellenar:

* **Project Name:** Nombre del activo. (Obligatorio)
* **Version:** Numero de versión de la app
* **Classfier:** Especifica el tipo de componente
* **Tags:** Tags para catalogación

Una vez creado ya tendremos nuestro proyecto donde iremos cargando las dependencias de nuestro proyecto con la tecnica [SBOM](../../ci/sbom/README.md)

![Nuevo proyecto](/img/dependency-track/new_project.png)

Para llevar un seguimiento de la evolución de la aplicación cuando se crea una nueva versión en el desarrollo, es necesario crear el mismo proyecto pero con una versión diferente. Esto permite agruparlos dentro del mismo proyecto, en el dashboard general se vera como una entrada independiente.

![Lista proyecto](/img/dependency-track/same_project.png)

Pero una vez dentro se agrupado por las versiones en el desplegable.

![Desplegable versiones](/img/dependency-track/group_project.png)
